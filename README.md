# bfres
A C++ library to (de)serialize the binary format "FRES" found in many Nintendo games.

# TODO
- Serialization back to binary
- General use functions
- Implementation of switch textures, which use the "BNTX" format
- Integrated conversion between WiiU and Switch

# Dependencies
- [binaryio](https://gitlab.com/Nitr4m12/binaryio)

# Credits
- @KillzXGaming: developing the [BfresLibrary](https://github.com/KillzXGaming/BfresLibrary) this one was inspired by, and also for helping with figuring out serialization.
- [wut](https://github.com/devkitPro/wut): source of `gx2/enum.h`

# License
- This library is licensed under the GNU General Public License v3+, and as such, it's provided free of charge with NO WARRANTY WHATSOEVER. For more info, see https://www.gnu.org/licenses/