#include "bfres/shapeanim/ResVertexShapeAnim.h"

namespace bfres::shapeanim {
void ResVertexShapeAnim::deserialize(ResParser& parser)
{
    auto num_curve {parser.read<std::uint8_t>()};
    auto num_key_shape_anim {parser.read<std::uint8_t>()};
    m_firstCurveIndex = parser.read<std::int32_t>();
    m_firstKeyShapeAnimIndex = parser.read<std::int32_t>();

    m_name = parser.parse_string_offset();

    m_shapeAnimKeyInfos =
        parser.parse_vector<ResShapeAnimKeyInfo>(num_key_shape_anim);
    m_curves = parser.parse_vector<ResAnimCurve>(num_curve);
    m_baseTransformValues = parser.parse_vector<float>(num_key_shape_anim - 1);
}

void ResVertexShapeAnim::serialize(ResBuilder &builder)
{
    builder.write<std::uint8_t>(m_curves.size());
    builder.write<std::uint8_t>(m_shapeAnimKeyInfos.size());
    builder.write<std::int32_t>(m_firstCurveIndex);
    builder.write<std::int32_t>(m_firstKeyShapeAnimIndex);

    builder.queue_string_table_entry(m_name);

    builder.queue_offset32(m_shapeAnimKeyInfos);
    builder.queue_offset32(m_curves);
    builder.queue_offset32(m_baseTransformValues);
}
}