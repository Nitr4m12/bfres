#include "bfres/shapeanim/ResShapeAnim.h"

namespace bfres::shapeanim {
void ResShapeAnim::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header {parser.read<BinaryBlockHeader>()};

        name = parser.parse_string_offset();
        path = parser.parse_string_offset();

        flags = parser.read<std::uint16_t>();
        numFrame = parser.read<std::int16_t>();

        auto num_vertex_shape_anim {parser.read<std::uint16_t>()};
        auto num_shape_anim_key {parser.read<std::uint8_t>()};
        auto num_curve {parser.read<std::uint8_t>()};

        parser.seek(2, std::ios_base::cur);

        bakedSize = parser.read<std::uint32_t>();

        auto num_user_data {parser.read<std::uint16_t>()};

        bindModel = parser.parse_offset<model::ResModel>();
        bindIndices = parser.parse_vector<std::uint16_t>(num_vertex_shape_anim);
        vertexShapeAnims =
            parser.parse_vector<ResVertexShapeAnim>(num_vertex_shape_anim);
        userData = parser.parse_dict_offset<ResUserData>();
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature {parser.read<std::uint32_t>()};
            flags = parser.read<std::uint32_t>();
        }
        else
            auto header {parser.read<BinaryFileHeader64>()};

        name = parser.parse_string_offset();
        path = parser.parse_string_offset();
        bindModel = parser.parse_offset<model::ResModel>();

        auto to_bind_indices {parser.read<std::uint64_t>()};
        auto to_vertex_shape_anims {parser.read<std::uint64_t>()};

        auto to_user_data {parser.read<std::uint64_t>()};
        userData = parser.parse_dict_offset<ResUserData>(to_user_data);

        if (parser.version_major2() < 9)
            flags = parser.read<std::int16_t>();

        auto num_user_data {parser.read<std::uint16_t>()};
        auto num_vertex_shape_anim {parser.read<std::uint16_t>()};
        auto num_key_shape_anim {parser.read<std::uint16_t>()};
        auto num_frames {parser.read<std::uint32_t>()};
        bakedSize = parser.read<std::uint32_t>();
        auto num_curve {parser.read<std::uint16_t>()};

        bindIndices = parser.parse_vector<std::uint16_t>(num_vertex_shape_anim,
                                                         to_bind_indices);
        vertexShapeAnims = parser.parse_vector<ResVertexShapeAnim>(
            num_vertex_shape_anim, to_vertex_shape_anims);
    }
}

// void ResShapeAnim::serialize(ResBuilder& builder) {}
} // namespace bfres