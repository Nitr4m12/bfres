#include "bfres/shapeanim/ResShapeAnimKeyInfo.h"

namespace bfres::shapeanim {
void ResShapeAnimKeyInfo::deserialize(ResParser& parser)
{
    m_curveIndex = parser.read<std::uint8_t>();
    m_subbindIndex = parser.read<std::uint8_t>();
    parser.seek(2, std::ios_base::cur);
    m_name = parser.parse_string_offset();
}

void ResShapeAnimKeyInfo::serialize(ResBuilder &builder)
{
    builder.write<std::uint8_t>(m_curveIndex);
    builder.write<std::uint8_t>(m_subbindIndex);
    builder.seek_cur(sizeof(std::uint16_t));
    builder.queue_string_table_entry(m_name);
}
}