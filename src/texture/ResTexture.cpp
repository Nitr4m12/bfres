#include "bfres/texture/ResTexture.h"

#include "bfres/common/ResCommon.h"

namespace bfres::texture {
void ResTexture::deserialize(ResParser& parser)
{
    auto header {parser.read<BinaryBlockHeader>()};

    m_gfxTexture = parser.read<GfxTexture>();

    m_handle = parser.read<std::uint32_t>();
    auto array_size {parser.read<std::uint32_t>()};

    m_name = parser.parse_string_offset();
    m_path = parser.parse_string_offset();

    if (parser.name().find(".Tex1") != std::string::npos) {
        m_data =
            parser.parse_vector<std::uint8_t>(m_gfxTexture.surface.imageSize);
        parser.seek(4, std::ios_base::cur);
    }
    else if (parser.name().find(".Tex2") != std::string::npos) {
        m_mipMapData =
            parser.parse_vector<std::uint8_t>(m_gfxTexture.surface.mipMapSize);
        parser.seek(4, std::ios_base::cur);
    }
    else {
        m_data =
            parser.parse_vector<std::uint8_t>(m_gfxTexture.surface.imageSize);
        m_mipMapData =
            parser.parse_vector<std::uint8_t>(m_gfxTexture.surface.mipMapSize);
    }
    m_userData = parser.parse_dict_offset<ResUserData>();

    auto num_user_data {parser.read<std::uint16_t>()};

    parser.seek(2, std::ios_base::cur);
}

void ResTexture::serialize(ResBuilder& builder) {}
} // namespace bfres