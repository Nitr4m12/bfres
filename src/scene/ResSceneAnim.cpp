#include "bfres/scene/ResSceneAnim.h"

namespace bfres::scene {
void ResSceneAnim::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header {parser.read<BinaryBlockHeader>()};
        m_name = parser.parse_string_offset();
        m_path = parser.parse_string_offset();

        auto num_user_data {parser.read<std::uint16_t>()};
        auto num_camera_anim {parser.read<std::uint16_t>()};
        auto num_light_anim {parser.read<std::uint16_t>()};
        auto num_fog_anim {parser.read<std::uint16_t>()};

        m_cameraAnims = parser.parse_dict_offset<scene::ResCameraAnim>();
        m_lightAnims = parser.parse_dict_offset<scene::ResLightAnim>();
        m_fogAnims = parser.parse_dict_offset<scene::ResFogAnim>();
        m_userData = parser.parse_dict_offset<ResUserData>();
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature {parser.read<std::uint32_t>()};
            auto flags {parser.read<std::uint32_t>()};
        }
        else
            auto header {parser.read<BinaryBlockHeader64>()};

        m_name = parser.parse_string_offset();
        m_path = parser.parse_string_offset();

        m_cameraAnims = parser.parse_offset_and_dict<scene::ResCameraAnim>();
        m_lightAnims = parser.parse_offset_and_dict<scene::ResLightAnim>();
        m_fogAnims = parser.parse_offset_and_dict<scene::ResFogAnim>();
        m_userData = parser.parse_offset_and_dict<ResUserData>();

        auto num_user_data {parser.read<std::uint16_t>()};
        auto num_camera_anims {parser.read<std::uint16_t>()};
        auto num_light_anims {parser.read<std::uint16_t>()};
        auto num_fog_anims {parser.read<std::uint16_t>()};
    }
}

void ResSceneAnim::serialize(ResBuilder& builder)
{
    builder.write_string("FSCN");
    if (builder.is_wiiu()) {
        builder.queue_string_table_entry(m_name);
        builder.queue_string_table_entry(m_path);

        builder.write_count(m_userData.size());
        builder.write_count(m_cameraAnims.size());
        builder.write_count(m_lightAnims.size());
        builder.write_count(m_fogAnims.size());

        builder.queue_offset32(m_cameraAnims);
        builder.queue_offset32(m_lightAnims);
        builder.queue_offset32(m_fogAnims);
        builder.queue_offset32(m_userData);
    }
    else {
        // if (builder.version_major2() >= 9)
        //     builder.write<std::uint32_t>(0);
        // else
        //     builder.write_header(440, 440);

        // builder.write_string_offset(m_name);
        // builder.write_string_offset(m_path);

        // builder.write_offset_and_dict(m_cameraAnims);
        // builder.write_offset_and_dict(m_lightAnims);
        // builder.write_offset_and_dict(m_fogAnims);
        // builder.write_offset_and_dict(m_userData);

        // builder.write_count(m_userData.size());
        // builder.write_count(m_cameraAnims.size());
        // builder.write_count(m_lightAnims.size());
        // builder.write_count(m_fogAnims.size());
    }
}
} // namespace bfres