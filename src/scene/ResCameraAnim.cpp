#include "bfres/scene/ResCameraAnim.h"

namespace bfres::scene {
void ResCameraAnim::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header {parser.read<BinaryBlockHeader>()};

        m_flags = parser.read<std::uint16_t>();

        parser.seek_cur(sizeof(std::uint16_t));

        auto num_frames {parser.read<int32_t>()};
        auto num_curve {parser.read<std::uint8_t>()};
        parser.seek_cur(sizeof(std::uint8_t));
        auto num_user_data {parser.read<std::uint16_t>()};

        m_bakedSize = parser.read<std::uint32_t>();
        m_name = parser.parse_string_offset();

        m_curves = parser.parse_vector<ResAnimCurve>(num_curve);
        m_baseValue = parser.parse_offset<CameraAnimResult>();
        m_userData = parser.parse_dict_offset<ResUserData>();
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature {parser.read<std::uint32_t>()};
            m_flags = parser.read<std::uint16_t>();
            parser.seek_cur(sizeof(std::uint16_t));
        }
        else
            auto header = parser.read<BinaryBlockHeader64>();

        m_name = parser.parse_string_offset();

        auto to_curves {parser.read<std::uint64_t>()};
        m_baseValue = parser.parse_offset<CameraAnimResult>();

        m_userData = parser.parse_offset_and_dict<ResUserData>();

        std::uint8_t num_curves;

        if (parser.version_major2() >= 9) {
            auto num_frames {parser.read<std::int32_t>()};
            m_bakedSize = parser.read<std::uint32_t>();
            auto num_user_data {parser.read<std::uint16_t>()};
            num_curves = parser.read<std::uint8_t>();
            parser.seek_cur(sizeof(std::uint32_t) + 1);
        }
        else {
            m_flags = parser.read<std::uint16_t>();
            parser.seek_cur(sizeof(std::uint16_t));
            auto num_frames {parser.read<std::int32_t>()};
            num_curves = parser.read<std::uint8_t>();
            parser.seek_cur(sizeof(std::int8_t));
            auto num_user_data {parser.read<std::uint16_t>()};
            m_bakedSize = parser.read<std::uint32_t>();
        }

        m_curves = parser.parse_vector<ResAnimCurve>(num_curves, to_curves);
    }
}

void ResCameraAnim::serialize(ResBuilder& builder)
{
    builder.write_string("FCAM");
    if (builder.is_wiiu()) {

        builder.write<std::uint16_t>(m_flags);

        builder.seek_cur(sizeof(std::uint16_t));

        uint num_frames {0};
        for (auto& curve : m_curves)
            num_frames += curve.frames().size();

        builder.write<int32_t>(num_frames);
        builder.write<std::uint8_t>(m_curves.size());
        builder.seek_cur(sizeof(std::uint8_t));
        builder.write<std::uint16_t>(m_userData.size());

        builder.write<std::uint32_t>(m_bakedSize);
        builder.queue_string_table_entry(m_name);

        builder.queue_offset32(m_curves);
        builder.queue_offset32(m_baseValue);
        builder.queue_offset32(m_userData);
    }
    // else {
    //     if (builder.version_major2() >= 9) {
    //         builder.write<std::uint16_t>(m_flags);
    //         builder.seek_cur(sizeof(std::uint16_t));
    //     } else {
    //         builder.write<std::uint32_t>(0);
    //         builder.write<std::uint32_t>(0);
    //         builder.seek_cur(sizeof(std::uint32_t));
    //     }

    //     builder.write_string_offset(m_name);
    //     builder.write_offset64(0);
    //     builder.write_value_offset(m_baseValue);

    //     builder.write_offset_and_dict(m_userData);

    //     if (builder.version_major2() >= 9) {
    //         builder.write<std::int32_t>(m_curves.size());
    //         builder.write<std::uint32_t>(m_bakedSize);
    //         builder.write<std::uint16_t>(m_userData.size());
    //         builder.write<std::uint8_t>(m_curves.size());
    //         builder.seek_cur(sizeof(std::uint32_t) + 1);
    //     }
    //     else {
    //         builder.write<std::uint16_t>(m_flags);
    //         builder.seek_cur(sizeof(std::uint16_t));
    //         builder.write<std::int32_t>(m_curves.size());
    //         builder.write<std::uint8_t>(m_curves.size());
    //         builder.seek_cur(sizeof(std::int8_t));
    //         builder.write<std::uint16_t>(m_userData.size());
    //         builder.write<std::uint32_t>(m_bakedSize);
    //     }
    // }
}
} // namespace bfres::scene