#include "bfres/scene/ResLightAnim.h"

namespace bfres::scene {
void ResLightAnim::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header {parser.read<BinaryBlockHeader>()};

        m_flags = parser.read<std::uint16_t>();

        auto num_user_data {parser.read<std::uint16_t>()};
        auto num_frames {parser.read<std::int32_t>()};
        auto num_curves {parser.read<std::uint8_t>()};

        m_lightType = parser.read<std::int8_t>();
        m_distAttenFuncIndex = parser.read<std::int8_t>();
        m_angleAttenFuncIndex = parser.read<std::int8_t>();

        m_bakedSize = parser.read<std::uint32_t>();

        m_name = parser.parse_string_offset();
        m_lightTypeName = parser.parse_string_offset();
        m_distAttenFuncName = parser.parse_string_offset();
        m_angleAttenFuncName = parser.parse_string_offset();

        m_curves = parser.parse_vector<ResAnimCurve>(num_curves);
        m_baseValue = parser.parse_offset<LightAnimResult>();
        m_userData = parser.parse_dict_offset<ResUserData>();
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature {parser.read<std::uint32_t>()};
            m_flags = parser.read<std::uint16_t>();
            parser.seek_cur(sizeof(std::uint16_t));
        }
        else
            auto header = parser.read<BinaryBlockHeader64>();

        m_name = parser.parse_string_offset();

        auto to_curves {parser.read<std::uint64_t>()};
        m_baseValue = parser.parse_offset<LightAnimResult>();

        auto to_user_data {parser.read<std::uint64_t>()};
        m_userData = parser.parse_dict_offset<ResUserData>(to_user_data);

        m_lightTypeName = parser.parse_string_offset();
        m_distAttenFuncName = parser.parse_string_offset();
        m_angleAttenFuncName = parser.parse_string_offset();

        if (parser.version_major2() < 9) {
            m_flags = parser.read<std::uint16_t>();
            auto num_user_data {parser.read<std::uint16_t>()};
        }

        auto num_frames {parser.read<std::int32_t>()};
        auto num_curves = parser.read<std::uint8_t>();
        m_lightType = parser.read<std::int8_t>();
        m_distAttenFuncIndex = parser.read<std::int8_t>();
        m_angleAttenFuncIndex = parser.read<std::int8_t>();
        m_bakedSize = parser.read<std::uint32_t>();

        if (parser.version_major2() >= 9) {
            auto num_user_data {parser.read<std::uint16_t>()};
            parser.seek_cur(sizeof(std::uint16_t));
        }

        m_curves = parser.parse_vector<ResAnimCurve>(num_curves, to_curves);
    }
}

void ResLightAnim::serialize(ResBuilder& builder)
{
    builder.write_string("FLIT");
    if (builder.is_wiiu()) {
        builder.write<std::uint16_t>(m_flags);

        uint num_frames {0};
        for (auto& curve : m_curves)
            num_frames += curve.frames().size();

        builder.write<std::uint16_t>(m_userData.size());
        builder.write<std::int32_t>(num_frames);
        builder.write<std::uint8_t>(m_curves.size());

        builder.write<std::int8_t>(m_lightType);
        builder.write<std::int8_t>(m_distAttenFuncIndex);
        builder.write<std::int8_t>(m_angleAttenFuncIndex);

        builder.write<std::uint32_t>(m_bakedSize);

        builder.queue_string_table_entry(m_name);
        builder.queue_string_table_entry(m_lightTypeName);
        builder.queue_string_table_entry(m_distAttenFuncName);
        builder.queue_string_table_entry(m_angleAttenFuncName);

        builder.queue_offset32(m_curves);
        builder.queue_offset32(m_baseValue);
        builder.queue_offset32(m_userData);
    }
    // else {
    //     builder.write_string("FLIT");
    //     if (builder.version_major2() >= 9) {
    //         builder.write<std::uint16_t>(m_flags);
    //         builder.seek_cur(sizeof(std::uint16_t));
    //     }
    //     else {
    //         builder.write<std::uint32_t>(0);
    //         builder.write<std::uint32_t>(0);
    //         builder.seek_cur(sizeof(std::uint32_t));
    //     }

    //     builder.write_string_offset(m_name);

    //     builder.write_offset64(0);
    //     builder.write_value_offset(m_baseValue);

    //     builder.write_offset_and_dict(m_userData);

    //     builder.write_string_offset(m_lightTypeName);
    //     builder.write_string_offset(m_distAttenFuncName);
    //     builder.write_string_offset(m_angleAttenFuncName);

    //     if (builder.version_major2() < 9) {
    //         builder.write<std::uint16_t>(m_flags);
    //         builder.write<std::uint16_t>(m_userData.size());
    //     }

    //     builder.write<std::int32_t>(m_curves.size());
    //     builder.write<std::uint8_t>(m_curves.size());

    //     builder.write<std::int8_t>(0);
    //     builder.write<std::int8_t>(0);
    //     builder.write<std::int8_t>(0);

    //     builder.write<std::uint32_t>(m_bakedSize);

    //     if (builder.version_major2() >= 9) {
    //         builder.write<std::uint16_t>(m_userData.size());
    //         builder.seek_cur(sizeof(std::uint16_t));
    //     }

    //     builder.write_vector_offset(m_curves);
    // }
}

} // namespace bfres::scene