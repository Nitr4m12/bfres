#include "bfres/shape/ResShape.h"

#include "bfres/common/ResCommon.h"

namespace bfres::shape {
void ResShape::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header {parser.read<BinaryBlockHeader>()};

        m_name = parser.parse_string_offset();

        m_flags = parser.read<std::uint32_t>();
        m_index = parser.read<std::uint16_t>();
        m_materialIndex = parser.read<std::uint16_t>();
        m_boneIndex = parser.read<std::uint16_t>();
        m_vertexIndex = parser.read<std::uint16_t>();

        auto num_skin_bone_index {parser.read<std::uint16_t>()};

        m_vtxSkinCount = parser.read<std::uint8_t>();

        auto num_mesh {parser.read<std::uint8_t>()};
        auto num_key_shape {parser.read<std::uint8_t>()};
        auto num_target_attrib {parser.read<std::uint8_t>()};
        auto num_bounding_node {parser.read<std::uint16_t>()};

        m_radius = parser.read<float>();

        m_vertex = parser.parse_offset<vertex::ResVertex>();
        m_meshes = parser.parse_vector<ResMesh>(num_mesh);
        m_skinBoneIndices =
            parser.parse_vector<std::uint16_t>(num_skin_bone_index);
        m_keyShapes = parser.parse_dict_offset<ResKeyShape>();

        if (num_bounding_node > 0) {
            m_boundingNodes =
                parser.parse_vector<ResBoundingNode>(num_bounding_node);
            m_subBoundings =
                parser.parse_vector<ResBounding>(num_bounding_node);
            m_subMeshIndices =
                parser.parse_vector<std::uint16_t>(num_bounding_node);
        }
        else {
            num_bounding_node = 0;
            if (parser.version() >= 0x04050000) {
                for (auto& mesh : m_meshes)
                    num_bounding_node += mesh.sub_meshes().size();
                num_bounding_node += m_meshes.size();
            }
            else {
                num_bounding_node = m_meshes[0].sub_meshes().size() + 2;
            }
            m_subBoundings =
                parser.parse_vector<ResBounding>(num_bounding_node);
        }

        auto user_ptr = parser.read<std::uint32_t>();
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature {parser.read<std::uint32_t>()};
            m_flags = parser.read<std::uint32_t>();
        }
        else {
            auto header = parser.read<BinaryBlockHeader64>();
        }

        m_name = parser.parse_string_offset();
        m_vertex = parser.parse_offset<vertex::ResVertex>();

        auto meshes_offset {parser.read_offset64()};
        auto skin_bone_indices_offset {parser.read_offset64()};
        m_keyShapes = parser.parse_offset_and_dict<ResKeyShape>();
        auto boundings_offset {parser.read_offset64()};

        std::uint64_t radius_offset;
        if (parser.version_major2() > 2 || parser.version_major() > 0) {
            radius_offset = parser.read_offset64();
            auto user_ptr = parser.read_offset64();
        }
        else {
            auto user_ptr = parser.read_offset64();
            m_radius = parser.read<float>();
        }

        if (parser.version_major2() < 9)
            m_flags = parser.read<std::uint32_t>();

        m_index = parser.read<std::uint16_t>();
        m_materialIndex = parser.read<std::uint16_t>();
        m_boneIndex = parser.read<std::uint16_t>();
        m_vertexIndex = parser.read<std::uint16_t>();

        auto num_skin_bone_index {parser.read_count()};
        m_vtxSkinCount = parser.read<std::uint8_t>();
        auto num_mesh = parser.read<std::uint8_t>();
        auto num_keys = parser.read<std::uint8_t>();
        auto num_target_attrib = parser.read<std::uint8_t>();

        if ((parser.version_major2() <= 2 && parser.version_major() == 0) ||
            parser.version_major2() >= 9)
            parser.seek_cur(sizeof(std::uint16_t));
        else
            parser.seek(sizeof(std::uint32_t) + 2);

        m_meshes = parser.parse_vector<ResMesh>(num_mesh, meshes_offset);
        m_skinBoneIndices = parser.parse_vector<std::uint16_t>(
            num_skin_bone_index, skin_bone_indices_offset);

        if (radius_offset != 0)
            // Incorporate into class
            auto radiuses {parser.parse_vector<float>(num_mesh, radius_offset)};

        // TODO: Make logic for parsing sub mesh boundings, a.k.a boundings
        int bounding_box_count;
    }
}

void ResShape::serialize(ResBuilder& builder)
{
    builder.write_string("FSHP");
    if (builder.is_wiiu()) {
        builder.queue_string_table_entry(m_name);

        builder.write<std::uint32_t>(m_flags);
        builder.write<std::uint16_t>(m_index);
        builder.write<std::uint16_t>(m_materialIndex);
        builder.write<std::uint16_t>(m_boneIndex);
        builder.write<std::uint16_t>(m_vertexIndex);

        builder.write_count(m_skinBoneIndices.size());

        builder.write<std::uint8_t>(m_vtxSkinCount);

        builder.write<std::uint8_t>(m_meshes.size());
        builder.write<std::uint8_t>(m_keyShapes.size());
        builder.write<std::uint8_t>(0); // TODO: Write num_target_attrib
        builder.write_count(m_boundingNodes.size());

        builder.write<float>(m_radius);

        builder.queue_offset32(m_vertex);
        builder.queue_offset32(m_meshes);
        builder.queue_offset32(m_skinBoneIndices);
        builder.queue_offset32(m_keyShapes);
        if (m_boundingNodes.size() > 0) {
            builder.queue_offset32(m_boundingNodes);
            builder.queue_offset32(m_subBoundings);
            builder.queue_offset32(m_subMeshIndices);
        }
        else {
            builder.queue_offset32(m_subBoundings);
        }


        // User Pointer, set at runtime
        builder.write<std::uint32_t>(0);

        builder.build_value_and_offset(m_meshes);
        builder.build_value_and_offset(m_skinBoneIndices);
        builder.build_dict_and_offset(m_keyShapes);

        if (m_boundingNodes.size() > 0) {
            builder.build_value_and_offset(m_boundingNodes);
            builder.build_value_and_offset(m_subBoundings);
            builder.build_value_and_offset(m_subMeshIndices);
        }
        else {
            builder.build_value_and_offset(m_subBoundings);

            // TODO: Unknown, find out what is this
            builder.write_offset64(0);
        }

        for (auto& mesh : m_meshes) {
            builder.queue_value(mesh.sub_meshes());
            for (auto& sub_mesh : mesh.sub_meshes()) {
                builder.build_value(sub_mesh);
            }
            builder.build_value_and_offset(mesh.index_buffer());
            builder.save_offset(mesh.sub_meshes());
        }
    }
}
} // namespace bfres