#include "bfres/shape/ResMesh.h"

namespace bfres::shape {
void ResMesh::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        m_primMode = parser.read<GX2PrimitiveMode>();
        m_type = parser.read<GX2IndexType>();

        auto index_count {parser.read<std::uint32_t>()};
        auto num_sub_mesh {parser.read<std::uint16_t>()};

        parser.seek_cur(2);

        m_subMeshes = parser.parse_vector<ResSubMesh>(num_sub_mesh);
        m_indexBuffer = parser.parse_offset<vertex::ResVertexBuffer>();

        // Offset to what?
        auto offset {parser.read<std::uint32_t>()};
    }
    else {
        auto to_sub_meshes {parser.read_offset64()};
        auto to_memory_pool {parser.read_offset64()};

        auto to_buffer {parser.read_offset64()};
        auto to_buffer_size {parser.read_offset64()};
        auto face_buffer_offset {parser.read<std::uint32_t>()};

        m_primMode = GX2PrimitiveMode(parser.read<std::uint32_t>());
        m_type = GX2IndexType(parser.read<std::uint32_t>());

        auto index_num {parser.read<std::uint32_t>()};
        auto first_vertex_index {parser.read<std::uint32_t>()};
        auto num_sub_mesh {parser.read_count()};

        parser.seek_cur(2);

        m_subMeshes =
            parser.parse_vector<ResSubMesh>(num_sub_mesh, to_sub_meshes);

        // data_offset;
    }
}

void ResMesh::serialize(ResBuilder& builder)
{
    if (builder.is_wiiu()) {
        builder.write<GX2PrimitiveMode>(m_primMode);
        builder.write<GX2IndexType>(m_type);

        builder.write<std::uint32_t>(0);
        builder.write_count(m_subMeshes.size());

        builder.seek_cur(sizeof(std::uint16_t));

        builder.queue_offset32(m_subMeshes);
        builder.queue_offset32(m_indexBuffer);

        // Offset of what?
        builder.write_offset32(0);
    }
    // else {
    //     builder.write_offset64(0);
    //     builder.write_offset64(0);

    //     builder.write_offset64(0);
    //     builder.write_offset64(0);
    //     builder.write<std::uint32_t>(0);

    //     builder.write<std::uint32_t>(m_primMode);
    //     builder.write<std::uint32_t>(m_type);

    //     builder.write<std::uint32_t>(0);
    //     builder.write<std::uint32_t>(0);

    //     builder.write_count(m_subMeshes.size());

    //     builder.seek_cur(sizeof(std::uint16_t));

    //     builder.write_vector_offset(m_subMeshes);
    // }
}
} // namespace bfres