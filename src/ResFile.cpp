#include "bfres/ResFile.h"

namespace bfres {
void ResFile::deserialize(std::string file_name)
{
    std::ifstream ifs {file_name};
    if (!ifs)
        throw std::runtime_error("File can't be opened!");
    ResParser parser {ifs};
    deserialize(parser);
}

void ResFile::deserialize(ResParser& parser)
{
    auto sig_word {parser.read<std::uint32_t>()};
    auto version {parser.read<std::uint32_t>()};
    parser.seek(0);

    if (version == 0x20202020) {
        auto header {parser.read<ResFileHeader64>()};
        m_version = header.version;
        parser.version(m_version);

        m_name = parser.parse_string_offset(header.toName);
        parser.name(m_name);

        m_models =
            parser.parse_vector<model::ResModel>(header.modelCount, header.toModels);
        m_skeletalAnims = parser.parse_vector<skeletalanim::ResSkeletalAnim>(
            header.skeletonAnimCount, header.toSkeletalAnims);
        m_matAnimArchives = parser.parse_vector<materialanim::ResMaterialAnimArchive>(
            header.materialAnimCount, header.toMaterialAnims);
        m_boneVisAnims = parser.parse_vector<visanim::ResVisibilityAnim>(
            header.boneVisAnimCount, header.toBoneVisibilityAnims);
        m_shapeAnims = parser.parse_vector<shapeanim::ResShapeAnim>(header.shapeAnimCount,
                                                         header.toShapeAnims);
        m_sceneAnims = parser.parse_vector<scene::ResSceneAnim>(header.sceneAnimCount,
                                                         header.toSceneAnims);
    }
    else {
        parser.is_wiiu(true);
        auto header {parser.read<BinaryFileHeader>()};
        m_version = header.version;
        parser.version(m_version);
        auto alignment {parser.read<std::uint32_t>()};

        m_name = parser.parse_string_offset();
        parser.name(m_name);

        auto string_table_size {parser.read<std::uint32_t>()};
        auto string_table_offset {parser.read_offset32()};

        m_models = parser.parse_dict_offset<model::ResModel>();
        m_textures = parser.parse_dict_offset<texture::ResTexture>();
        m_skeletalAnims = parser.parse_dict_offset<skeletalanim::ResSkeletalAnim>();
        m_shaderParamAnims = parser.parse_dict_offset<materialanim::ResShaderParamAnim>();
        m_colorAnims = parser.parse_dict_offset<materialanim::ResShaderParamAnim>();
        m_textureSrts = parser.parse_dict_offset<materialanim::ResShaderParamAnim>();
        m_texturePatternAnims =
            parser.parse_dict_offset<materialanim::ResTexturePatternAnim>();
        m_boneVisAnims = parser.parse_dict_offset<visanim::ResVisibilityAnim>();
        m_matVisAnims = parser.parse_dict_offset<visanim::ResVisibilityAnim>();
        m_shapeAnims = parser.parse_dict_offset<shapeanim::ResShapeAnim>();
        m_sceneAnims = parser.parse_dict_offset<scene::ResSceneAnim>();
        m_externalFiles = parser.parse_dict_offset<ResExternalFileData>();

        auto num_model {parser.read<std::uint16_t>()};
        auto num_texture {parser.read<std::uint16_t>()};
        auto num_skeletal_anim {parser.read<std::uint16_t>()};
        auto num_shader_param {parser.read<std::uint16_t>()};
        auto num_color_anim {parser.read<std::uint16_t>()};
        auto num_texture_srt_anim {parser.read<std::uint16_t>()};
        auto num_texture_pattern_anim {parser.read<std::uint16_t>()};
        auto num_bone_vis_anim {parser.read<std::uint16_t>()};
        auto num_mat_vis_anim {parser.read<std::uint16_t>()};
        auto num_shape_anim {parser.read<std::uint16_t>()};
        auto num_scene_anim {parser.read<std::uint16_t>()};
        auto num_external_file {parser.read<std::uint16_t>()};

        auto user_ptr {parser.read<std::uint32_t>()};

        auto embedded {parser.read<ResExternalFileData>()};
    }

    m_stringTable = parser.string_table();
}

void ResFile::serialize()
{
    std::string fname {m_name + "_wiiu.bfres"};
    std::ofstream ofs {fname};
    ResBuilder builder {ofs};
    builder.is_wiiu(true);
    serialize(builder);
}

void ResFile::serialize(ResBuilder& builder)
{
    builder.set_string_table(m_stringTable);
    if (builder.is_wiiu()) {
        builder.write_string("FRES");
        builder.write<std::uint32_t>(m_version);
        builder.write<std::uint16_t>(0xFEFF);
        builder.write<std::uint16_t>(0x10);
        // File Size. Written after the full file is done
        builder.write<std::uint32_t>(0);

        builder.version(m_version);

        // Alignment
        builder.write<std::uint32_t>(0x100);

        builder.queue_string_table_entry(m_name);

        // String table size and offset. Written
        // after the full file is done
        builder.write<std::uint32_t>(0);
        builder.queue_offset32(m_stringTable);

        builder.queue_offset32(m_models);
        builder.queue_offset32(m_textures);
        builder.queue_offset32(m_skeletalAnims);
        builder.queue_offset32(m_shaderParamAnims);
        builder.queue_offset32(m_colorAnims);
        builder.queue_offset32(m_textureSrts);
        builder.queue_offset32(m_texturePatternAnims);
        builder.queue_offset32(m_boneVisAnims);
        builder.queue_offset32(m_matVisAnims);
        builder.queue_offset32(m_shapeAnims);
        builder.queue_offset32(m_sceneAnims);
        builder.queue_offset32(m_externalFiles);

        builder.write_count(m_models.size());
        builder.write_count(m_textures.size());
        builder.write_count(m_skeletalAnims.size());
        builder.write_count(m_shaderParamAnims.size());
        builder.write_count(m_colorAnims.size());
        builder.write_count(m_textureSrts.size());
        builder.write_count(m_texturePatternAnims.size());
        builder.write_count(m_boneVisAnims.size());
        builder.write_count(m_matVisAnims.size());
        builder.write_count(m_shapeAnims.size());
        builder.write_count(m_sceneAnims.size());
        builder.write_count(m_externalFiles.size());


        // User Pointer, set at runtime
        builder.write<std::int32_t>(0);

        builder.build_values(m_externalFiles);

        builder.build_dict_and_offset(m_models);
        builder.build_dict_and_offset(m_textures);
        builder.build_dict_and_offset(m_skeletalAnims);
        builder.build_dict_and_offset(m_shaderParamAnims);
        builder.build_dict_and_offset(m_colorAnims);
        builder.build_dict_and_offset(m_textureSrts);
        builder.build_dict_and_offset(m_texturePatternAnims);
        builder.build_dict_and_offset(m_boneVisAnims);
        builder.build_dict_and_offset(m_matVisAnims);
        builder.build_dict_and_offset(m_shapeAnims);
        builder.build_dict_and_offset(m_sceneAnims);
        builder.build_dict_and_offset(m_externalFiles);

        builder.build_values_and_offsets(m_models);
        builder.build_values_and_offsets(m_textures);
        builder.build_values_and_offsets(m_skeletalAnims);
        builder.build_values_and_offsets(m_shaderParamAnims);
        builder.build_values_and_offsets(m_colorAnims);
        builder.build_values_and_offsets(m_textureSrts);
        builder.build_values_and_offsets(m_texturePatternAnims);
        builder.build_values_and_offsets(m_boneVisAnims);
        builder.build_values_and_offsets(m_matVisAnims);
        builder.build_values_and_offsets(m_shapeAnims);
        builder.build_values_and_offsets(m_sceneAnims);

        builder.save_offsets(m_externalFiles);

        builder.build_string_table();
    }
}
} // namespace bfres