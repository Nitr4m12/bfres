#include "bfres/common/ResAnimCurve.h"

namespace bfres {
void ResAnimCurve::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        m_flags = parser.read<std::uint16_t>();

        m_frameType = AnimCurveFrameType(m_flags & frameTypeMask);
        m_keyType = AnimCurveKeyType((m_flags & keyTypeMask) >> 2);
        m_curveType = AnimCurveType((m_flags & curveTypeMask) >> 4);

        int num_key = {parser.read<std::uint16_t>()};

        m_targetOffset = parser.read<std::uint32_t>();
        m_startFrame = parser.read<float>();
        m_endFrame = parser.read<float>();

        m_dataScale = parser.read<float>();
        m_dataOffset = parser.read<float>();

        if (parser.version() >= 0x03040000)
            m_dataDelta = parser.read<float>();

        switch (m_frameType) {
        case AnimCurveFrameType::Float:
        {
            auto frames = parser.parse_vector<float>(num_key);
            for (auto& frame : frames)
                m_frames.push_back(frame);
        }
        case AnimCurveFrameType::FixedPoint16:
        {
            auto frames = parser.parse_vector<FixedPoint16>(num_key);
            for (auto& frame : frames)
                m_frames.push_back(frame);
        }
        case AnimCurveFrameType::Byte:
        {
            auto frames = parser.parse_vector<std::uint8_t>(num_key);
            for (auto& frame : frames)
                m_frames.push_back(frame);
        }
        default:
            throw std::runtime_error(
                "bfres::ResAnimCurve::deserialize(): Invalid curve frame type");
            break;
        }

        std::size_t original_pos {parser.tell()};
        std::int32_t keys_offset {parser.read<std::int32_t>()};

        parser.seek(original_pos + keys_offset);
        m_keys.resize(num_key);

        int key_size = elements_per_key();

        switch (m_keyType) {
        case AnimCurveKeyType::Float:
            for (int i {0}; i < num_key; ++i) {
                m_keys[i].resize(key_size);
                for (int j {0}; j < key_size; ++j)
                    m_keys[i][j] = parser.read<float>();
            }
            break;
        case AnimCurveKeyType::Int16:
            for (int i {0}; i < num_key; ++i) {
                m_keys[i].resize(key_size);
                for (int j {0}; j < key_size; ++j)
                    m_keys[i][j] = parser.read<std::int16_t>();
            }
            break;
        case AnimCurveKeyType::SByte:
            for (int i {0}; i < num_key; ++i) {
                m_keys[i].resize(key_size);
                for (int j {0}; j < key_size; ++j)
                    m_keys[i][j] = parser.read<std::int8_t>();
            }
            break;
        default:
            throw std::runtime_error(
                "bfres::ResAnimCurve::deserialize(): Invalid curve key type");
            break;
        }

        parser.seek(original_pos + sizeof(std::int32_t));

        // if (m_curveType == AnimCurveType::StepBool) {
        //     int key_index = 0;

        //     m_keyStepBoolData.resize(num_key);

        //     for (int i {0}; i < m_keys.size(); ++i) {
        //         if (num_key <= key_index)
        //             break;

        //         auto value = m_keys[i][0];

        //         for (int j = 0; j < 32; j++) {
        //             if (num_key <= key_index)
        //                 break;

        //             bool set = (value & 0x1) != 0;
        //             value >>= 1;

        //             m_keyStepBoolData[key_index] = set;
        //             key_index++;
        //         }
        //     }
        // }
    }
    else {
        auto frames_offset {parser.read_offset64()};
        auto keys_offset {parser.read_offset64()};
        m_flags = parser.read<std::uint16_t>();
        auto num_key {parser.read_count()};
        auto anim_data_offset = parser.read<std::uint32_t>();

        m_startFrame = parser.read<float>();
        m_endFrame = parser.read<float>();
        m_dataScale = parser.read<float>();
        m_dataOffset = parser.read<float>();
        m_dataDelta = parser.read<float>();
        parser.seek_cur(sizeof(std::uint32_t));
    }
}

void ResAnimCurve::serialize(ResBuilder& builder)
{
    if (builder.is_wiiu()) {
        builder.write<std::uint16_t>(m_flags);
        builder.write<std::uint16_t>(m_keys.size());
        builder.write<std::uint32_t>(m_targetOffset);
        builder.write<float>(m_startFrame);
        builder.write<float>(m_endFrame);
        builder.write<float>(m_dataScale);
        builder.write<float>(m_dataOffset);
        if (builder.version() >= 0x03040000)
            builder.write<float>(m_dataDelta);

        // Frames offset
        builder.write_offset32(builder.tell() + sizeof(std::int32_t)*2);

        // Keys offset
        builder.write_offset32(builder.tell() + sizeof(std::int32_t));

        switch (m_frameType) {
        case AnimCurveFrameType::Float:
            for (auto& frame : m_frames)
                builder.write<float>(std::get<float>(frame));
            break;
        case AnimCurveFrameType::FixedPoint16:
            for (auto& frame : m_frames)
                builder.write<FixedPoint16>(std::get<FixedPoint16>(frame));
            break;
        case AnimCurveFrameType::Byte:
            for (auto& frame : m_frames)
                builder.write<std::uint8_t>(std::get<std::uint8_t>(frame));
            break;
        default:
            throw std::runtime_error(
                "bfres::ResAnimCurve::serialize(): Can't write frames of unknown type");
            break;
        }

        switch (m_keyType) {
        case AnimCurveKeyType::Float:
            for (auto& key : m_keys) {
                for (auto& element : key)
                    builder.write<float>(std::get<float>(element));
            }
            break;
        case AnimCurveKeyType::Int16:
            for (auto& key : m_keys) {
                for (auto& element : key)
                    builder.write<std::int16_t>(std::get<std::int16_t>(element));
            }
            break;
        case AnimCurveKeyType::SByte:
            for (auto& key : m_keys) {
                for (auto& element : key)
                    builder.write<std::int8_t>(std::get<std::int8_t>(element));
            }
            break;
        default:
            throw std::runtime_error(
                "bfres::ResAnimCurve::serialize(): Can't write keys of unknown type");
            break;
        }

    }
    else {
        builder.write_offset64(0);
        builder.write_offset64(0);
        builder.write<std::uint16_t>(m_flags);
        builder.write_count(m_keys.size());
        builder.write_offset64(0);

        builder.write<float>(m_startFrame);
        builder.write<float>(m_endFrame);
        builder.write<float>(m_dataScale);
        builder.write<float>(m_dataOffset);
        builder.write<float>(m_dataDelta);
        builder.seek_cur(sizeof(std::uint32_t));
    }
}
} // namespace bfres