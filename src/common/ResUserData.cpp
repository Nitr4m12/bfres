#include "bfres/common/ResUserData.h"

namespace bfres {
void ResUserData::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        m_name = parser.parse_string_offset();
        auto data_count {parser.read<std::uint16_t>()};
        auto type {parser.read<std::uint8_t>()};
        parser.seek(1, std::ios_base::cur);

        switch (type) {
        case 0:
            m_data = parser.parse_vector<std::int32_t>(data_count);
            break;
        case 1:
            m_data = parser.parse_vector<float>(data_count);
            break;
        case 2:
        case 3:
            m_data = parser.parse_vector_of_strings(data_count);
            break;
        case 4:
            m_data = parser.parse_vector<UserDataStruct>(data_count);
            break;
        default:
            throw std::runtime_error(
                "ResUserData::deserialize(): Invalid user data type");
            break;
        }
    }
    else {
        m_name = parser.parse_string_offset();

        auto data_offset {parser.read_offset64()};
        if (parser.version_major2() <= 2 && parser.version_major() == 0)
            parser.seek_cur(sizeof(std::uint64_t));

        auto data_count {parser.read<std::uint32_t>()};
        auto type {parser.read<std::uint32_t>()};

        if (parser.version_major2() > 2 || parser.version_major() != 0)
            parser.seek_cur(sizeof(std::uint64_t) * 5 + 3);

        switch (type) {
        case 0:
            m_data = parser.parse_vector<std::int32_t>(data_count);
            break;
        case 1:
            m_data = parser.parse_vector<float>(data_count);
            break;
        case 2:
        case 3:
            m_data = parser.parse_vector_of_strings(data_count);
            break;
        case 4:
            m_data = parser.parse_vector<UserDataStruct>(data_count);
            break;
        default:
            throw std::runtime_error(
                "ResUserData::deserialize(): Invalid user data type");
            break;
        }
    }
}

void ResUserData::serialize(ResBuilder& builder) {}
} // namespace bfres