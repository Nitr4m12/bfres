#include "bfres/ResParser.h"

namespace bfres {
// Public methods

ResParser::ResParser(std::istream& is)
    : binaryio::BinaryReader(is, binaryio::endian::little) {};

// ResParser::ResParser(std::istream& is, std::shared_ptr<ResFile> file)
//     : binaryio::BinaryReader(is, binaryio::endian::little)
// {
//     if (file)
//         m_mainFile = file;
//     else
//         m_mainFile = std::shared_ptr<ResFile> {new ResFile()};
// };

void ResParser::seek_cur(const std::size_t offset)
{
    this->seek(offset, std::ios_base::cur);
}

std::int32_t ResParser::read_offset32() { return this->read<std::int32_t>(); }
std::uint64_t ResParser::read_offset64() { return this->read<std::uint64_t>(); }
std::uint16_t ResParser::read_count() { return this->read<std::uint16_t>(); }

template <typename T>
T ResParser::parse_offset()
{
    T val;
    std::size_t original_pos {go_to_offset()};
    if (original_pos == 0)
        return val;

    deserialize_value(val);

    go_back(original_pos, true);
    return val;
}

template <typename T>
T ResParser::parse_offset(const std::size_t& offset)
{
    T val;
    std::size_t original_pos {go_to_offset(offset)};
    if (original_pos == 0)
        return val;

    deserialize_value(val);

    go_back(original_pos, false);
    return val;
}

std::string ResParser::parse_string_offset()
{
    std::string str_val;

    std::size_t original_pos {go_to_offset()};
    if (original_pos == 0)
        return str_val;

    parse_string(str_val);

    go_back(original_pos, true);
    return str_val;
}

std::string ResParser::parse_string_offset(const std::size_t& offset)
{
    std::string str_val;

    std::size_t original_pos {go_to_offset(offset)};
    if (original_pos == 0)
        return str_val;

    parse_string(str_val);

    go_back(original_pos, false);
    return str_val;
}

template <typename T>
std::vector<T> ResParser::parse_vector(const int& count)
{
    std::vector<T> vals;

    std::size_t original_pos {go_to_offset()};
    if (original_pos == 0)
        return vals;

    vals.resize(count);
    for (int i {0}; i < vals.size(); ++i)
        deserialize_value(vals[i]);

    go_back(original_pos, true);
    return vals;
}

template <typename T>
std::vector<T> ResParser::parse_vector(const int& count,
                                       const std::size_t& offset)
{
    std::vector<T> vals;

    std::size_t original_pos {go_to_offset(offset)};
    if (original_pos == 0)
        return vals;

    vals.resize(count);
    for (int i {0}; i < vals.size(); ++i)
        deserialize_value(vals[i]);

    go_back(original_pos, false);
    return vals;
}

std::vector<std::string> ResParser::parse_vector_of_strings(const int& count)
{
    std::vector<std::string> strings;

    std::size_t original_pos {go_to_offset()};
    if (original_pos == 0)
        return strings;

    strings.resize(count);
    for (int i {0}; i < strings.size(); ++i)
        strings[i] = this->read_string();

    go_back(original_pos, true);
    return strings;
}

std::vector<std::string>
ResParser::parse_vector_of_strings(const int& count, const std::size_t& offset)
{
    std::vector<std::string> strings;

    std::size_t original_pos {go_to_offset(offset)};
    if (original_pos == 0)
        return strings;

    strings.resize(count);
    for (int i {0}; i < strings.size(); ++i)
        strings[i] = this->read_string();

    go_back(original_pos, false);
    return strings;
}

template <typename T>
std::vector<T> ResParser::parse_offset_and_dict()
{
    std::vector<T> vals;
    auto to_vals {this->read<std::uint64_t>()};
    auto original_pos {go_to_offset()};
    if (original_pos == 0)
        return vals;

    parse_dict(vals, to_vals);

    go_back(original_pos, true);
    return vals;
}

template <typename T>
std::vector<T> ResParser::parse_dict_offset()
{
    std::vector<T> vals;

    std::size_t original_pos {go_to_offset()};
    if (original_pos == 0)
        return vals;

    parse_dict(vals);

    go_back(original_pos, true);
    return vals;
};

template <typename T>
std::vector<T> ResParser::parse_dict_offset(const std::size_t& vals_offset)
{
    std::vector<T> vals;

    std::size_t original_pos {go_to_offset()};
    if (original_pos == 0)
        return vals;

    parse_dict(vals, vals_offset);

    go_back(original_pos, true);
    return vals;
};

template <typename T>
std::vector<T> ResParser::parse_dict_offset(const std::size_t& vals_offset,
                                            const std::size_t& offset)
{
    std::vector<T> vals;

    std::size_t original_pos {go_to_offset(offset)};
    if (original_pos == 0)
        return vals;

    parse_dict(vals, vals_offset);

    go_back(original_pos, false);
    return vals;
}

int ResParser::version() const { return m_version; }
void ResParser::version(int ver) { m_version = ver; }

int ResParser::version_major() const { return version() >> 24; }
int ResParser::version_major2() const { return version() >> 16 & 0xFF; }
int ResParser::version_minor() const { return version() >> 8 & 0xFF; }
int ResParser::version_micro() const { return version() & 0xFF; }

bool ResParser::ptr_set()
{
    if (!m_mainFile)
        return false;
    return true;
}

std::string ResParser::name() const { return m_name; }
void ResParser::name(std::string n) { m_name = n; }

bool ResParser::is_wiiu() const { return m_wiiu; }
void ResParser::is_wiiu(bool val)
{
    m_wiiu = val;
    if (m_wiiu)
        this->set_endianness(binaryio::endian::big);
    else
        this->set_endianness(binaryio::endian::little);
}

auto ResParser::get_file_ptr() const { return m_mainFile; }

// ----------------------------- Private methods -----------------------------

std::size_t ResParser::go_to_offset()
{
    std::size_t original_pos {this->tell()};
    if (m_wiiu) {
        auto offset {this->read<std::int32_t>()};
        if (offset == -1 || offset == 0) {
            this->seek(original_pos + offset_size());
            return 0;
        }

        this->seek(original_pos + offset);
    }
    else {
        auto offset {this->read<std::uint64_t>()};
        if (offset == -1 || offset == 0) {
            this->seek(original_pos + offset_size());
            return 0;
        }

        this->seek(offset);
    }
    return original_pos;
}

std::size_t ResParser::go_to_offset(const std::size_t& abs_offset)
{
    std::size_t original_pos {this->tell()};
    if (abs_offset == 0)
        return 0;
    this->seek(abs_offset);
    return original_pos;
}

std::size_t ResParser::offset_size() const
{
    if (m_wiiu)
        return sizeof(std::int32_t);
    else
        return sizeof(std::uint64_t);
}

void ResParser::go_back(const std::size_t& original_pos, const bool with_offset)
{
    if (with_offset)
        this->seek(original_pos + offset_size());
    else
        this->seek(original_pos);
}

template <typename T>
void ResParser::deserialize_value(T& val)
{
    if constexpr (std::is_base_of_v<IResData, T>)
        val.deserialize(*this);
    else
        val = this->read<T>();
}

void ResParser::parse_string(std::string& str_val)
{
    std::uint32_t str_size;

    if (m_wiiu) {
        this->seek(this->tell() - sizeof(std::uint32_t));
        str_size = this->read<std::uint32_t>() + 1;
    }
    else {
        str_size = this->read<std::uint16_t>() + 1;
    }

    str_val = this->read_string(str_size);
}

template <typename T>
void ResParser::parse_dict()
{
    auto dict_size {this->read<std::uint32_t>()};
    auto num_nodes {this->read<std::int32_t>()};
    for (int i {0}; i < num_nodes + 1; ++i) {
        auto ref_bit {this->read<std::uint32_t>()};
        auto left_index {this->read<std::uint16_t>()};
        auto right_index {this->read<std::uint16_t>()};
        auto name_offset {this->read<std::int32_t>()};
    }
}

template <typename T>
void ResParser::parse_dict(std::vector<T>& vals)
{
    auto dict_size = this->read<std::uint32_t>();
    auto num_nodes = this->read<std::int32_t>();
    vals.resize(num_nodes + 1);
    if (m_wiiu) {
        for (int i {0}; i < vals.size(); ++i) {
            auto ref_bit {this->read<std::uint32_t>()};
            auto left_index {this->read<std::uint16_t>()};
            auto right_index {this->read<std::uint16_t>()};
            auto name_offset {this->read<std::int32_t>()};
            vals[i] = parse_offset<T>();
        }
    }
}

template <typename T>
void ResParser::parse_dict(std::vector<T>& vals, const std::size_t& vals_offset)
{
    auto dict_size = this->read<std::uint32_t>();
    auto num_nodes = this->read<std::int32_t>();
    vals.resize(num_nodes + 1);
    for (int i {0}; i < vals.size(); ++i) {
        auto ref_bit {this->read<std::uint32_t>()};
        auto left_index {this->read<std::uint16_t>()};
        auto right_index {this->read<std::uint16_t>()};
        auto name_offset {this->read<std::uint64_t>()};
        if (i > 0)
            vals[i] = parse_offset<T>(vals_offset);
    }
}

} // namespace bfres