#include "bfres/material/ResShaderParam.h"

namespace bfres::material {
void ResShaderParam::deserialize(ResParser& parser)
{
    std::size_t shader_param_start = parser.tell();
    std::uint16_t data_offset {0};
    if (parser.is_wiiu()) {

        m_type = parser.read<std::uint8_t>();
        m_dataSize = parser.read<std::uint8_t>();
        data_offset = parser.read<std::uint16_t>();
        std::int32_t uni_var_offset = parser.read<std::int32_t>();

        if (parser.version() >= 0x03030000) {
            m_conversionCallbackPtr = parser.read<std::uint32_t>();
            m_dependedIndex = parser.read<std::uint16_t>();
            m_dependIndex = parser.read<std::uint16_t>();
        }

        if (parser.version() >= 0x3030000 && parser.version() < 0x03040000)
            std::int32_t fmatOffset = parser.read<std::uint32_t>();

        m_name = parser.parse_string_offset();
    }
    else {
        m_conversionCallbackPtr = parser.read_offset64();
        m_name = parser.parse_string_offset();

        m_type = parser.read<std::uint8_t>();
        m_dataSize = parser.read<std::uint8_t>();
        data_offset = parser.read<std::uint16_t>();
        std::int32_t uni_var_offset = parser.read_offset64();

        m_dependedIndex = parser.read<std::uint16_t>();
        m_dependIndex = parser.read<std::uint16_t>();
        parser.seek_cur(sizeof(std::uint32_t));
    }

    if (data_offset == 0 || data_offset == 0xFFFF)
        return;

    parser.seek(shader_param_start + data_offset);
    switch (m_type) {
    case 0:
        m_data = (bool)parser.read<float>();
        break;
    case 1:
        m_data = parser.read<Vector2<float>>();
        break;
    case 2:
        m_data = parser.read<Vector3<float>>();
        break;
    case 3:
        m_data = parser.read<Vector4<float>>();
        break;
    case 4:
        m_data = parser.read<int>();
        break;
    case 5:
        m_data = parser.read<Vector2<int>>();
        break;
    case 6:
        m_data = parser.read<Vector3<int>>();
        break;
    case 7:
        m_data = parser.read<Vector4<int>>();
        break;
    case 8:
        m_data = parser.read<unsigned int>();
        break;
    case 9:
        m_data = parser.read<Vector2<unsigned int>>();
        break;
    case 10:
        m_data = parser.read<Vector3<unsigned int>>();
        break;
    case 11:
        m_data = parser.read<Vector4<unsigned int>>();
        break;
    case 12:
        m_data = parser.read<float>();
        break;
    case 13:
        m_data = parser.read<Vector2<float>>();
        break;
    case 14:
        m_data = parser.read<Vector3<float>>();
        break;
    case 15:
        m_data = parser.read<Vector4<float>>();
        break;
    case 17:
        m_data = parser.read<Matrix<float, 2, 2>>();
        break;
    case 18:
        m_data = parser.read<Matrix<float, 2, 3>>();
        break;
    case 19:
        m_data = parser.read<Matrix<float, 2, 4>>();
        break;
    case 21:
        m_data = parser.read<Matrix<float, 3, 2>>();
        break;
    case 22:
        m_data = parser.read<Matrix<float, 3, 3>>();
        break;
    case 23:
        m_data = parser.read<Matrix<float, 3, 4>>();
        break;
    case 25:
        m_data = parser.read<Matrix<float, 4, 2>>();
        break;
    case 26:
        m_data = parser.read<Matrix<float, 4, 3>>();
        break;
    case 27:
        m_data = parser.read<Matrix<float, 4, 4>>();
        break;
    case 28:
        m_data = parser.read<Srt2d>();
        break;
    case 29:
        m_data = parser.read<Srt3d>();
        break;
    case 30:
        m_data = parser.read<TextureSrt>();
        break;
    case 31:
        m_data = parser.read<TextureSrtEx>();
        break;
    default:
        throw std::runtime_error(
            "ResShaderParam::deserialize(): Unknown data type");
        break;
    }
}

void ResShaderParam::serialize(ResBuilder& builder)
{
    std::size_t shader_param_start = builder.tell();
    std::uint16_t data_offset {0};
    if (builder.is_wiiu()) {

        builder.write<std::uint8_t>(m_type);
        builder.write<std::uint8_t>(m_dataSize);
        builder.write<std::uint16_t>(data_offset);
        builder.write<std::int32_t>(0);

        if (builder.version() >= 0x03030000) {
            builder.write<std::uint32_t>(m_conversionCallbackPtr);
            builder.write<std::uint16_t>(m_dependedIndex);
            builder.write<std::uint16_t>(m_dependIndex);
        }

        if (builder.version() >= 0x3030000 && builder.version() < 0x03040000)
            builder.write<std::uint32_t>(0);

        builder.queue_string_table_entry(m_name);
    }
    else {
        builder.write_offset64(m_conversionCallbackPtr);
        builder.queue_string_table_entry(m_name);

        builder.write<std::uint8_t>(m_type);
        builder.write<std::uint8_t>(m_dataSize);
        builder.write<std::uint16_t>(data_offset);
        builder.write_offset64(0);

        builder.write<std::uint16_t>(m_dependedIndex);
        builder.write<std::uint16_t>(m_dependIndex);
        builder.seek_cur(sizeof(std::uint32_t));
    }

    if (data_offset == 0 || data_offset == 0xFFFF)
        return;

    builder.seek(shader_param_start + data_offset);
    switch (m_type) {
    case 0:
        builder.write<float>(std::get<bool>(m_data));
        break;
    case 1:
        builder.write<Vector2<float>>(std::get<Vector2<float>>(m_data));
        break;
    case 2:
        builder.write<Vector3<float>>(std::get<Vector3<float>>(m_data));
        break;
    case 3:
        builder.write<Vector4<float>>(std::get<Vector4<float>>(m_data));
        break;
    case 4:
        builder.write<int>(std::get<int>(m_data));
        break;
    case 5:
        builder.write<Vector2<int>>(std::get<Vector2<int>>(m_data));
        break;
    case 6:
        builder.write<Vector3<int>>(std::get<Vector3<int>>(m_data));
        break;
    case 7:
        builder.write<Vector4<int>>(std::get<Vector4<int>>(m_data));
        break;
    case 8:
        builder.write<unsigned int>(std::get<unsigned int>(m_data));
        break;
    case 9:
        builder.write<Vector2<unsigned int>>(std::get<Vector2<unsigned int>>(m_data));
        break;
    case 10:
        builder.write<Vector3<unsigned int>>(std::get<Vector3<unsigned int>>(m_data));
        break;
    case 11:
        builder.write<Vector4<unsigned int>>(std::get<Vector4<unsigned int>>(m_data));
        break;
    case 12:
        builder.write<float>(std::get<float>(m_data));
        break;
    case 13:
        builder.write<Vector2<float>>(std::get<Vector2<float>>(m_data));
        break;
    case 14:
        builder.write<Vector3<float>>(std::get<Vector3<float>>(m_data));
        break;
    case 15:
        builder.write<Vector4<float>>(std::get<Vector4<float>>(m_data));
        break;
    case 17:
        builder.write<Matrix<float, 2, 2>>(std::get<Matrix<float, 2, 2>>(m_data));
        break;
    case 18:
        builder.write<Matrix<float, 2, 3>>(std::get<Matrix<float, 2, 3>>(m_data));
        break;
    case 19:
        builder.write<Matrix<float, 2, 4>>(std::get<Matrix<float, 2, 4>>(m_data));
        break;
    case 21:
        builder.write<Matrix<float, 3, 2>>(std::get<Matrix<float, 3, 2>>(m_data));
        break;
    case 22:
        builder.write<Matrix<float, 3, 3>>(std::get<Matrix<float, 3, 3>>(m_data));
        break;
    case 23:
        builder.write<Matrix<float, 3, 4>>(std::get<Matrix<float, 3, 4>>(m_data));
        break;
    case 25:
        builder.write<Matrix<float, 4, 2>>(std::get<Matrix<float, 4, 2>>(m_data));
        break;
    case 26:
        builder.write<Matrix<float, 4, 3>>(std::get<Matrix<float, 4, 3>>(m_data));
        break;
    case 27:
        builder.write<Matrix<float, 4, 4>>(std::get<Matrix<float, 4, 4>>(m_data));
        break;
    case 28:
        builder.write<Srt2d>(std::get<Srt2d>(m_data));
        break;
    case 29:
        builder.write<Srt3d>(std::get<Srt3d>(m_data));
        break;
    case 30:
        builder.write<TextureSrt>(std::get<TextureSrt>(m_data));
        break;
    case 31:
        builder.write<TextureSrtEx>(std::get<TextureSrtEx>(m_data));
        break;
    default:
        throw std::runtime_error(
            "ResShaderParam::deserialize(): Can't write unknown data type");
        break;
    }
}
} // namespace bfres::material