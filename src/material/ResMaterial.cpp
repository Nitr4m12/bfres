#include <cmath>

#include "bfres/material/ResMaterial.h"

namespace bfres::material {
void ResMaterial::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header {parser.read<BinaryBlockHeader>()};
        m_name = parser.parse_string_offset();

        m_flags = parser.read<std::uint32_t>();

        auto section_index {parser.read<std::uint16_t>()};
        auto num_render_info {parser.read<std::uint16_t>()};
        auto num_texture {parser.read<std::uint8_t>()};
        auto num_sampler {parser.read<std::uint8_t>()};
        auto num_shader_param {parser.read<std::uint16_t>()};
        auto num_shader_param_volatile {parser.read<std::uint16_t>()};
        auto src_param_size_in_bytes {parser.read<std::uint16_t>()};
        auto raw_param_size {parser.read<std::uint16_t>()};
        auto num_user_data {parser.read<std::uint16_t>()};

        m_renderInfos = parser.parse_dict_offset<material::ResRenderInfo>();
        m_renderState = parser.parse_offset<ResRenderState>();
        m_shaderAssign = parser.parse_offset<material::ResShaderAssign>();
        m_textureRefs = parser.parse_vector<texture::ResTextureRef>(num_texture);
        m_samplers = parser.parse_vector<material::ResSampler>(num_sampler);
        auto to_sampler_dict {parser.read_offset32()};
        m_shaderParams =
            parser.parse_vector<material::ResShaderParam>(num_shader_param);
        auto to_shader_params_dict {parser.read_offset32()};
        auto to_shader_param_data {parser.read_offset32()};

        m_userData = parser.parse_dict_offset<ResUserData>();

        m_volatileFlags =
            parser.parse_vector<std::uint8_t>(std::ceil(num_shader_param / 8));
        auto user_ptr {parser.read_offset32()};
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature {parser.read_many<char>(4)};
            m_flags = parser.read<std::uint32_t>();
        }
        else
            auto header {parser.read<BinaryBlockHeader64>()};

        m_name = parser.parse_string_offset();

        // TODO: VersionMajor2 10 parsing

        m_renderInfos = parser.parse_offset_and_dict<material::ResRenderInfo>();
        m_shaderAssign = parser.parse_offset<material::ResShaderAssign>();
        auto to_textures {parser.read_offset64()};
        auto to_texture_names {parser.read_offset64()};

        auto unk = parser.read_offset64();

        m_samplers = parser.parse_offset_and_dict<material::ResSampler>();
        m_shaderParams =
            parser.parse_offset_and_dict<material::ResShaderParam>();
        auto src_param_offset {parser.read_offset64()};
        m_userData = parser.parse_offset_and_dict<ResUserData>();

        auto to_volatile_flags {parser.read_offset64()};
        auto user_ptr {parser.read_offset64()};
        auto to_sampler_slots {parser.read_offset64()};
        auto to_texture_slots {parser.read_offset64()};

        if (parser.version_major2() < 9)
            m_flags = parser.read<std::uint32_t>();

        auto index {parser.read<std::uint16_t>()};
        auto num_render_info {parser.read<std::uint16_t>()};
        auto num_texture_ref {parser.read<std::uint8_t>()};
        auto num_sampler {parser.read<std::uint8_t>()};
        auto num_shader_param {parser.read<std::uint16_t>()};
        auto num_shader_param_volatile {parser.read<std::uint16_t>()};
        auto src_param_size {parser.read<std::uint16_t>()};
        auto raw_param_size {parser.read<std::uint16_t>()};
        auto num_user_data {parser.read<std::uint16_t>()};

        if (parser.version_major2() < 9)
            parser.seek_cur(sizeof(std::uint32_t));

        auto texture_names {
            parser.parse_vector_of_strings(num_texture_ref, to_texture_names)};

        m_textureRefs.resize(num_texture_ref);

        // TODO: Set up ResTextureRef as a class
        // for (int i{0}; i < m_textureRefs.size(); ++i)
        //     m_textureRefs[i].name = texture_names[i];

        // TODO
        //  for (auto& sampler : m_samplers)
        //  sampler.

        auto shader_param_data =
            parser.parse_vector<std::uint8_t>(src_param_size, src_param_offset);

        m_volatileFlags = parser.parse_vector<std::uint8_t>(
            std::ceil(num_shader_param / 8), to_volatile_flags);
        auto texture_slots = parser.parse_vector<std::uint64_t>(
            num_texture_ref, to_sampler_slots);
        auto sampler_slots =
            parser.parse_vector<std::uint64_t>(num_sampler, to_texture_slots);
    }
}

void ResMaterial::serialize(ResBuilder& builder)
{
    builder.write_string("FMAT");
    if (builder.is_wiiu()) {
        builder.queue_string_table_entry(m_name);

        builder.write<std::uint32_t>(m_flags);

        builder.write<std::uint16_t>(0); // TODO: Write index

        builder.write<std::uint16_t>(m_renderInfos.size());
        builder.write<std::uint8_t>(m_textureRefs.size());
        builder.write<std::uint8_t>(m_samplers.size());
        builder.write_count(m_shaderParams.size());
        builder.write_count(m_volatileFlags.size());
        builder.write_count(m_shaderParams.size() * sizeof(material::ResShaderParam));
        builder.write_count(0);
        builder.write_count(m_userData.size());

        builder.queue_offset32(m_renderInfos);
        builder.queue_offset32(m_renderState);
        builder.queue_offset32(m_shaderAssign);
        builder.queue_offset32(m_textureRefs);
        builder.queue_offset32(m_samplers);
        builder.write_offset32(0); // Samplers dict
        builder.queue_offset32(m_shaderParams);
        builder.write_offset32(0); // Shader Params dict
        builder.write_offset32(0); // Shader Params data

        builder.queue_offset32(m_userData);

        builder.queue_offset32(m_volatileFlags);

        // User Pointer, set at runtime
        builder.write_offset32(0);

        builder.build_values(m_renderInfos);
        builder.build_value_and_offset(m_renderState);
        builder.build_value_and_offset(m_textureRefs);
        builder.build_value_and_offset(m_samplers);
        builder.build_value_and_offset(m_shaderParams);
        builder.build_value_and_offset(m_shaderAssign);
    }
    else {
        // if (builder.version_major2() >= 9)
        //     builder.write<std::uint32_t>(m_flags);
        // else {
        //     builder.write<std::uint32_t>(0);
        //     builder.write<std::uint32_t>(0);
        //     builder.seek_cur(0);
        // }

        // builder.write_string_offset(m_name);

        // builder.write_offset_and_dict(m_renderInfos);
        // builder.write_value_offset(m_shaderAssign);
        // builder.write_offset64(0);
        // builder.write_offset64(0);

        // builder.write_offset64(0);

        // builder.write_offset_and_dict(m_samplers);
        // builder.write_offset_and_dict(m_shaderParams);
        // builder.write_offset64(0);
        // builder.write_offset_and_dict(m_userData);

        // builder.write_offset64(0);
        // builder.write_offset64(0);
        // builder.write_offset64(0);
        // builder.write_offset64(0);

        // if (builder.version_major2() < 9)
        //     builder.write<std::uint32_t>(m_flags);

        // builder.write<std::uint16_t>(0);
        // builder.write<std::uint16_t>(m_renderInfos.size());
        // builder.write<std::uint8_t>(m_textureRefs.size());
        // builder.write<std::uint8_t>(m_samplers.size());
        // builder.write<std::uint16_t>(m_shaderParams.size());
        // builder.write<std::uint16_t>(m_shaderParams.size());
        // builder.write<std::uint16_t>(0);
        // builder.write<std::uint16_t>(0);
        // builder.write<std::uint16_t>(m_userData.size());

        // if (builder.version_major2() < 9)
        //     builder.seek_cur(sizeof(std::uint32_t));
    }
}

} // namespace bfres