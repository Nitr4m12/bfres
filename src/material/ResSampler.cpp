#include "bfres/material/ResSampler.h"

namespace bfres::material {
void ResSampler::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        m_gfxSampler = parser.read<GX2Sampler>();
        m_name = parser.parse_string_offset();
        auto index {parser.read<std::int8_t>()};
        parser.seek_cur(sizeof(std::uint16_t) + 1);
    }
    else {
        // TODO: Actually parse the switch version

        auto wrapModeX {parser.read<std::uint8_t>()};
        auto wrapModeY {parser.read<std::uint8_t>()};
        auto wrapModeZ {parser.read<std::uint8_t>()};
        auto compareFunc {parser.read<std::uint8_t>()};
        auto borderColorType {parser.read<std::uint8_t>()};
        auto anisotropic {parser.read<std::uint8_t>()};
        auto filter_flags {parser.read<std::uint16_t>()};
        auto minLod {parser.read<float>()};
        auto maxLod {parser.read<float>()};
        auto LODBias {parser.read<float>()};
    }
}

void ResSampler::serialize(ResBuilder& builder)
{
    if (builder.is_wiiu()) {
        builder.write<GX2Sampler>(m_gfxSampler);

        builder.queue_string_table_entry(m_name);

        // TODO: Write correct index
        builder.write<std::int8_t>(0);
        builder.seek_cur(sizeof(std::uint16_t) + 1);
    }
    // else {
    //     // TODO: Actually write the switch version

    //     builder.write<std::uint8_t>(0);
    //     builder.write<std::uint8_t>(0);
    //     builder.write<std::uint8_t>(0);
    //     builder.write<std::uint8_t>(0);
    //     builder.write<std::uint8_t>(0);
    //     builder.write<std::uint8_t>(0);
    //     builder.write<std::uint16_t>(0);
    //     builder.write<float>(0);
    //     builder.write<float>(0);
    //     builder.write<float>(0);

    // }
}
} // namespace bfres::material