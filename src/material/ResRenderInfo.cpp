#include "bfres/material/ResRenderInfo.h"

namespace bfres::material {
void ResRenderInfo::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        int array_size {parser.read<std::uint16_t>()};
        m_data.resize(array_size);

        int val_type {parser.read<std::uint8_t>()};
        parser.seek_cur(1);
        m_varName = parser.parse_string_offset();

        switch (val_type) {
        case 0:
            for (int i {0}; i < m_data.size(); ++i)
                m_data[i] = parser.read<Vector2f>();
            break;
        case 1:
            for (int i {0}; i < m_data.size(); ++i)
                m_data[i] = parser.read<Vector2i>();
            break;
        case 2:
            for (int i {0}; i < m_data.size(); ++i)
                m_data[i] = parser.parse_string_offset();
            break;
        default:
            throw std::runtime_error(
                "ResRenderInfo::deserialize(): Invalid data type");
        }
    }
    else {
        m_varName = parser.parse_string_offset();

        auto to_data {parser.read_offset64()};
        auto data_count {parser.read_count()};
        auto val_type {parser.read<std::uint8_t>()};

        parser.seek_cur(sizeof(std::uint32_t) + 1);

        m_data.resize(data_count);

        auto original_pos {parser.tell()};
        parser.seek(to_data);

        switch (val_type) {
        case 0:
            for (int i {0}; i < m_data.size(); ++i)
                m_data[i] = parser.read<Vector2f>();
            break;
        case 1:
            for (int i {0}; i < m_data.size(); ++i)
                m_data[i] = parser.read<Vector2i>();
            break;
        case 2:
            for (int i {0}; i < m_data.size(); ++i)
                m_data[i] = parser.parse_string_offset();
            break;
        default:
            throw std::runtime_error(
                "ResRenderInfo::deserialize(): Invalid data type");
        }
        parser.seek(original_pos);
    }
}

void ResRenderInfo::serialize(ResBuilder& builder)
{
    if (builder.is_wiiu()) {
        builder.write<std::uint16_t>(m_data.size());

        if (m_data.size() > 0) {
            switch (m_data[0].index()) {
            case 0:
                builder.write<std::uint8_t>(0);
                builder.seek_cur(sizeof(std::uint8_t));

                builder.queue_string_table_entry(m_varName);

                for (auto& data : m_data)
                    builder.write<Vector2f>(std::get<Vector2f>(data));

                break;
            case 1:
                builder.write<std::uint8_t>(1);
                builder.seek_cur(sizeof(std::uint8_t));

                builder.queue_string_table_entry(m_varName);

                for (auto& data : m_data)
                    builder.write<Vector2i>(std::get<Vector2i>(data));

                break;
            case 2:
                builder.write<std::uint8_t>(2);
                builder.seek_cur(sizeof(std::uint8_t));

                builder.queue_string_table_entry(m_varName);

                for (auto& data : m_data)
                    builder.queue_string_table_entry(std::get<std::string>(data));

                break;
            default:
                throw std::runtime_error(
                    "ResRenderInfo::deserialize(): Can't write invalid data type");
                break;
            }
        }
        else {
            builder.write<std::uint8_t>(2);
            builder.queue_string_table_entry(m_varName);
            for (auto& data : m_data)
                builder.queue_string_table_entry(std::get<std::string>(data));
        }
    }
}
} // namespace bfres::material