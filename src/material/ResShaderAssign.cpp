#include "bfres/material/ResShaderAssign.h"

namespace bfres::material {
void ResShaderAssign::deserialize(ResParser& parser)
{
    m_shaderArchiveName = parser.parse_string_offset();
    m_shadingModelName = parser.parse_string_offset();

    if (!parser.is_wiiu()) {
        m_attribAssigns = parser.parse_offset_and_dict<std::array<char, 3>>();
        m_samplerAssigns = parser.parse_offset_and_dict<std::array<char, 3>>();
        m_shaderOptions = parser.parse_offset_and_dict<std::array<char, 3>>();
    }

    auto rev {parser.read<std::uint32_t>()};

    auto num_attrib_assign {parser.read<std::uint8_t>()};
    auto num_sampler_assign {parser.read<std::uint8_t>()};
    auto num_shader_option {parser.read<std::uint16_t>()};

    if (parser.is_wiiu()) {
        m_attribAssigns = parser.parse_dict_offset<std::array<char, 3>>();
        m_samplerAssigns = parser.parse_dict_offset<std::array<char, 3>>();
        m_shaderOptions = parser.parse_dict_offset<std::array<char, 3>>();
    }
}

void ResShaderAssign::serialize(ResBuilder& builder)
{
    builder.queue_string_table_entry(m_shaderArchiveName);
    builder.queue_string_table_entry(m_shadingModelName);

    if (!builder.is_wiiu()) {
        // builder.write_offset_and_dict(m_attribAssigns);
        // builder.write_offset_and_dict(m_samplerAssigns);
        // builder.write_offset_and_dict(m_shaderOptions);
    }

    // TODO: check what revision we are dealing with
    builder.write<std::uint8_t>(0);

    builder.write<std::uint8_t>(m_attribAssigns.size());
    builder.write<std::uint8_t>(m_samplerAssigns.size());
    builder.write<std::uint8_t>(m_shaderOptions.size());

    if (builder.is_wiiu()) {
        builder.queue_offset32(m_attribAssigns);
        builder.queue_offset32(m_samplerAssigns);
        builder.queue_offset32(m_shaderOptions);
    }
}
} // namespace bfres::material