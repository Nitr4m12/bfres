#include <cmath>

#include "bfres/visibilityanim/ResVisibilityAnim.h"

namespace bfres::visanim {
void ResVisibilityAnim::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header {parser.read<BinaryBlockHeader>()};

        m_name = parser.parse_string_offset();
        m_path = parser.parse_string_offset();

        m_flags = parser.read<std::uint16_t>();

        auto num_user_data {parser.read<std::uint16_t>()};
        auto num_frame {parser.read<int32_t>()};
        auto num_anims {parser.read<uint16_t>()};
        auto num_curve {parser.read<std::uint16_t>()};

        m_bakedSize = parser.read<std::uint32_t>();

        m_bindModel = parser.parse_offset<model::ResModel>();
        m_bindIndices = parser.parse_vector<std::uint16_t>(num_anims);
        m_names = parser.parse_vector_of_strings(num_anims);
        m_curves = parser.parse_vector<ResAnimCurve>(num_curve);
        m_baseValues =
            parser.parse_vector<std::uint8_t>(std::ceil(num_anims / 8));
        m_userData = parser.parse_dict_offset<ResUserData>();
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature {parser.read<std::uint32_t>()};
            m_flags = parser.read<std::uint16_t>();
            parser.seek(sizeof(std::uint16_t), std::ios_base::cur);
        }
        else
            auto header = parser.read<BinaryFileHeader64>();

        m_name = parser.parse_string_offset();
        m_path = parser.parse_string_offset();
        m_bindModel = parser.parse_offset<model::ResModel>();

        auto to_bind_indices {parser.read<std::uint64_t>()};
        auto to_curves {parser.read<std::uint64_t>()};
        auto to_base_values {parser.read<std::uint64_t>()};
        auto to_names {parser.read<std::uint64_t>()};

        auto to_user_data {parser.read<std::uint64_t>()};
        m_userData = parser.parse_dict_offset<ResUserData>(to_user_data);

        if (parser.version_major2() < 9)
            m_flags = parser.read<std::uint16_t>();

        auto num_user_data {parser.read<std::uint16_t>()};
        auto num_frames {parser.read<std::int32_t>()};
        auto num_anims {parser.read<std::uint16_t>()};
        auto num_curves {parser.read<std::uint16_t>()};

        m_bakedSize = parser.read<std::uint32_t>();

        m_bindIndices =
            parser.parse_vector<std::uint16_t>(num_anims, to_bind_indices);
        m_names = parser.parse_vector_of_strings(num_anims, to_names);
        m_curves = parser.parse_vector<ResAnimCurve>(num_curves, to_curves);

        // TODO: parse base values
    }
}

void ResVisibilityAnim::serialize(ResBuilder& builder) {}
} // namespace bfres