#include "bfres/skeletalanim/ResSkeletalAnim.h"

namespace bfres::skeletalanim {
void ResSkeletalAnim::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header {parser.read<BinaryBlockHeader>()};

        m_name = parser.parse_string_offset();
        m_path = parser.parse_string_offset();

        m_flags = parser.read<std::uint32_t>();

        std::uint32_t num_frame;
        std::uint16_t num_bone_anim;
        std::uint16_t num_user_data;
        std::uint32_t num_curve;

        if (parser.version() >= 0x03040000) {
            num_frame = parser.read<std::uint32_t>();
            num_bone_anim = parser.read<std::uint16_t>();
            num_user_data = parser.read<std::uint16_t>();
            num_curve = parser.read<std::uint32_t>();
        }
        else {
            num_frame = parser.read<std::uint16_t>();
            num_bone_anim = parser.read<std::uint16_t>();
            num_user_data = parser.read<std::uint16_t>();
            num_curve = parser.read<std::uint16_t>();
            parser.seek(2, std::ios_base::cur);
        }

        m_bakedSize = parser.read<std::uint32_t>();

        m_boneAnims = parser.parse_vector<ResBoneAnim>(num_bone_anim);
        m_bindSkeleton = parser.parse_offset<skeleton::ResSkeleton>();
        m_bindIndices = parser.parse_vector<std::int16_t>(num_bone_anim);
        m_userData = parser.parse_dict_offset<ResUserData>();
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature = parser.read<uint32_t>();
            m_flags = parser.read<std::uint32_t>();
        }
        else
            auto header = parser.read<BinaryBlockHeader64>();

        m_name = parser.parse_string_offset();
        m_path = parser.parse_string_offset();
        m_bindSkeleton = parser.parse_offset<skeleton::ResSkeleton>();

        auto to_bind_indices {parser.read<std::uint64_t>()};
        auto to_bone_anims {parser.read<std::uint64_t>()};

        m_userData = parser.parse_offset_and_dict<ResUserData>();

        if (parser.version_major2() < 9)
            m_flags = parser.read<std::uint32_t>();

        auto num_frames {parser.read<std::int32_t>()};
        auto num_curves {parser.read<std::int32_t>()};
        m_bakedSize = parser.read<std::uint32_t>();
        auto num_bone_anims {parser.read<std::uint16_t>()};
        auto num_user_data {parser.read<std::uint16_t>()};

        if (parser.version_major2() < 9)
            parser.seek_cur(sizeof(std::uint32_t));

        m_boneAnims =
            parser.parse_vector<ResBoneAnim>(num_bone_anims, to_bone_anims);
        m_bindIndices =
            parser.parse_vector<std::int16_t>(num_bone_anims, to_bind_indices);

    }
}

void ResSkeletalAnim::serialize(ResBuilder& builder)
{
    builder.write_string("FSKA");
    if (builder.is_wiiu()) {
        builder.queue_string_table_entry(m_name); // name
        builder.queue_string_table_entry(m_path); // path
        builder.write<std::uint32_t>(m_flags);    // flags

        int num_curves {0};
        int num_frames {0};

        for (auto& anim : m_boneAnims) {
            num_curves += anim.curves().size();
            for (auto& curve : anim.curves())
                num_frames += curve.frames().size();
        }

        if (builder.version() >= 0x03040000) {
            builder.write<std::uint32_t>(num_frames);
            builder.write<std::uint16_t>(m_boneAnims.size());
            builder.write<std::uint16_t>(m_userData.size());
            builder.write<std::uint32_t>(num_curves);
        }
        else {
            builder.write<std::uint16_t>(num_frames);
            builder.write<std::uint16_t>(m_boneAnims.size());
            builder.write<std::uint16_t>(m_userData.size());
            builder.write<std::uint16_t>(num_curves);
            builder.seek_cur(sizeof(std::uint16_t));
        }

        builder.write<std::uint32_t>(m_bakedSize);

        auto offset_to_bone_anims {builder.tell()};
        builder.seek_cur(sizeof(std::int32_t));

        builder.queue_offset32(m_bindSkeleton);
        builder.queue_offset32(m_bindIndices);
        builder.queue_offset32(m_userData);

        builder.build_value_and_offset(m_bindIndices);
        builder.align_up(4);

        auto bone_anims_pos {builder.tell()};
        std::size_t next_anim_result {bone_anims_pos +
                                      0x18 * m_boneAnims.size()};
        for (auto& bone_anim : m_boneAnims) {
            // Bone anims are stored in an array of bone anims,
            // followed by an array of base values and curves
            // mapped to the bone anim in the corresponding index

            bone_anim.serialize(builder);

            std::size_t offset_to_curves {builder.tell()};
            builder.seek_cur(sizeof(std::int32_t));

            builder.write_offset32(next_anim_result - builder.tell());

            auto next_bone_anim {builder.tell()};

            builder.seek(next_anim_result);
            bone_anim.base_value().serialize(builder);

            std::size_t curves_start_pos {builder.tell()};
            for (auto& curve : bone_anim.curves())
                curve.serialize(builder);

            next_anim_result = builder.tell();

            builder.seek(offset_to_curves);
            builder.write_offset32(curves_start_pos - builder.tell());

            builder.seek(next_bone_anim);
        }

        builder.seek(offset_to_bone_anims);
        builder.write_offset32(bone_anims_pos - offset_to_bone_anims);
    }
}
} // namespace bfres