#include "bfres/skeletalanim/ResBoneAnimResult.h"

namespace bfres::skeletalanim {
void ResBoneAnimResult::deserialize(ResParser& parser)
{
    if (((m_flags & 0b00001000) >> 3) == 1)
        m_scale = parser.read<Vector3f>();

    if (((m_flags & 0b00010000) >> 4) == 1)
        m_rotation = parser.read<Vector4f>();

    if (((m_flags & 0b00100000)) >> 5 == 1)
        m_translation = parser.read<Vector3f>();
}

void ResBoneAnimResult::serialize(ResBuilder& builder)
{
    if (((m_flags & 0b00001000) >> 3) == 1)
        builder.write<Vector3f>(m_scale);

    if (((m_flags & 0b00010000) >> 4) == 1)
        builder.write<Vector4f>(m_rotation);

    if (((m_flags & 0b00100000)) >> 5 == 1)
        builder.write<Vector3f>(m_translation);
}
}