#include "bfres/skeletalanim/ResBoneAnim.h"

namespace bfres::skeletalanim {
void ResBoneAnim::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        m_flags = parser.read<std::uint32_t>();

        m_baseValue.flags(m_flags);

        m_name = parser.parse_string_offset();

        m_initialRotation = parser.read<std::uint8_t>();
        m_initialTranslation = parser.read<std::uint8_t>();

        auto num_curve = parser.read<std::uint8_t>();

        m_baseTranslateOffset = parser.read<std::uint8_t>();
        m_startCurveIndex = parser.read<std::uint8_t>();

        parser.seek(3, std::ios_base::cur);

        m_curves = parser.parse_vector<ResAnimCurve>(num_curve);
        m_baseValue = parser.parse_offset<ResBoneAnimResult>();
    }
    else {
        m_name = parser.parse_string_offset();
        auto to_curves {parser.read<std::uint64_t>()};
        auto to_base_value {parser.read<std::uint64_t>()};


        if (parser.version_major2() >= 9)
            parser.seek(sizeof(std::uint64_t) * 2, std::ios_base::cur);

        m_flags = parser.read<std::uint32_t>();
        m_baseValue.flags(m_flags);

        m_initialRotation = parser.read<std::uint8_t>();
        m_initialTranslation = parser.read<std::uint8_t>();
        auto num_curves = parser.read<std::uint8_t>();
        m_baseTranslateOffset = parser.read<std::uint8_t>();
        m_startCurveIndex = parser.read<std::int32_t>();
        parser.seek(sizeof(std::int32_t), std::ios_base::cur);

        m_curves = parser.parse_vector<ResAnimCurve>(num_curves, to_curves);
        m_baseValue = parser.parse_offset<ResBoneAnimResult>(to_base_value);
    }
}

void ResBoneAnim::serialize(ResBuilder &builder)
{
    if (builder.is_wiiu()) {
        builder.write<std::uint32_t>(m_flags);

        builder.queue_string_table_entry(m_name);

        builder.write<std::uint8_t>(m_initialRotation);
        builder.write<std::uint8_t>(m_initialTranslation);
        builder.write<std::uint8_t>(m_curves.size());
        builder.write<std::uint8_t>(m_baseTranslateOffset);
        builder.write<std::uint8_t>(m_startCurveIndex);
        builder.seek_cur(sizeof(std::uint16_t)+1);

        // Curves and base values are serialized by skeleton anims
    }
}
}