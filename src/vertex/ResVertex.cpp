#include "bfres/vertex/ResVertex.h"

#include "bfres/common/ResCommon.h"

namespace bfres::vertex {
void ResVertex::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header {parser.read<BinaryBlockHeader>()};

        auto num_vtx_attrib {parser.read<std::uint8_t>()};
        auto num_vtx_buffer {parser.read<std::uint8_t>()};
        auto index {parser.read<std::uint16_t>()};
        auto count {parser.read<std::uint32_t>()};

        m_vtxSkinCount = parser.read<std::uint8_t>();
        parser.seek_cur(sizeof(std::uint16_t) + 1);

        m_vtxAttribs = parser.parse_vector<ResVertexAttrib>(num_vtx_attrib);

        // For serializing the dict later
        m_vtxAttribsDictDummy = parser.read<std::int32_t>();

        m_vtxBuffer = parser.parse_vector<ResVertexBuffer>(num_vtx_buffer);

        auto user_ptr {parser.read<std::int32_t>()};
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature {parser.read<std::uint32_t>()};
            auto flags {parser.read<std::uint32_t>()};
        }
        else {
            auto header {parser.read<BinaryBlockHeader64>()};
        }

        m_vtxAttribs = parser.parse_offset_and_dict<ResVertexAttrib>();

        auto to_memory_pool {parser.read_offset64()};

        auto unk {parser.read_offset64()};
        if (parser.version_major2() > 2 || parser.version_major() > 0)
            parser.read_offset64();

        auto to_vertex_buffer_size {parser.read_offset64()};
        auto to_vertex_stride_size {parser.read_offset64()};
        parser.seek(sizeof(std::uint64_t), std::ios_base::cur);

        auto buffer_offset {parser.read<std::int32_t>()};

        auto num_vtx_attrib {parser.read<std::uint8_t>()};
        auto num_vtx_buffer {parser.read<std::uint8_t>()};
        auto index {parser.read<std::uint16_t>()};
        auto num_vertex {parser.read<std::uint32_t>()};

        m_vtxSkinCount = parser.read<std::uint32_t>();

        // TODO: add parsing of "stride" and "buffer size" arrays
    }
}
void ResVertex::serialize(ResBuilder& builder, int index)
{
    builder.write_string("FVTX");
    if (builder.is_wiiu()) {
        builder.write<std::uint8_t>(m_vtxAttribs.size());
        builder.write<std::uint8_t>(m_vtxBuffer.size());
        builder.write<std::uint16_t>(index);
        builder.write<std::uint32_t>(0);

        builder.write<std::uint8_t>(m_vtxSkinCount);

        builder.seek_cur(sizeof(std::uint16_t) + 1);
        builder.queue_offset32(m_vtxAttribs);
        builder.queue_offset32(m_vtxAttribsDictDummy);

        builder.queue_offset32(m_vtxBuffer);

        // User Pointer, set at runtime
        builder.write_offset32(0);
    }
    // else {
    //     if (builder.version_major2() >= 9)
    //         builder.write<std::uint32_t>(0);
    //     else {
    //         builder.write<std::uint32_t>(0);
    //         builder.write<std::uint32_t>(0);
    //     }

    //     builder.write_offset_and_dict(m_vtxAttribs);
    //     builder.write_offset64(0);

    //     builder.write_offset64(0);

    //     if (builder.version_major2() > 2 || builder.version_major() > 0)
    //         builder.seek_cur(sizeof(std::uint64_t));

    //     builder.write_offset64(0);
    //     builder.write_offset64(0);
    //     builder.seek_cur(sizeof(std::uint64_t));

    //     builder.write<std::int32_t>(0);

    //     builder.write<std::uint8_t>(m_vtxAttribs.size());
    //     builder.write<std::uint8_t>(m_vtxBuffer.size());
    //     builder.write<std::uint16_t>(0);
    //     builder.write<std::uint32_t>(0);

    //     builder.write<std::uint32_t>(m_vtxSkinCount);
    // }
}
} // namespace bfres