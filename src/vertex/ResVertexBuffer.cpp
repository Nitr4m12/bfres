#include "bfres/vertex/ResVertexBuffer.h"

namespace bfres::vertex {
void ResVertexBuffer::deserialize(ResParser& parser)
{
    m_gfxBuffer = parser.read<ResBuffer>();
    // parser.seek(4, std::ios_base::cur);
    // data = byte[gfxBuffer.numBuffering][gfxBuffer.size]
    m_data = parser.parse_vector<std::uint8_t>(m_gfxBuffer.size);
}

void ResVertexBuffer::serialize(ResBuilder &builder)
{
    builder.write<ResBuffer>(m_gfxBuffer);
    builder.queue_offset32(m_data);
}
}