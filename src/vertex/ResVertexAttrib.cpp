#include "bfres/vertex/ResVertexAttrib.h"

namespace bfres::vertex {
void ResVertexAttrib::deserialize(ResParser& parser)
{
    m_name = parser.parse_string_offset();
    if (parser.is_wiiu()) {
        m_bufferIndex = parser.read<std::uint8_t>();
        parser.seek(1, std::ios_base::cur);
        m_elementOffset = parser.read<std::uint16_t>();
        m_format = parser.read<GX2AttribFormat>();
    }
    else {
        parser.set_endianness(binaryio::endian::big);
        m_format = GX2AttribFormat(parser.read<std::uint16_t>());
        parser.swap_endianness();
        parser.seek(sizeof(std::uint16_t), std::ios_base::cur);
        m_elementOffset = parser.read<std::uint16_t>();
        m_bufferIndex = parser.read<std::uint16_t>();
    }
}

void ResVertexAttrib::serialize(ResBuilder& builder)
{
    builder.queue_string_table_entry(m_name);
    if (builder.is_wiiu()) {
        builder.write<std::uint8_t>(m_bufferIndex);
        builder.seek_cur(sizeof(std::int8_t));
        builder.write<std::uint16_t>(m_elementOffset);
        builder.write<GX2AttribFormat>(m_format);
    }
    else {
        builder.set_endianness(binaryio::endian::big);
        builder.write<std::uint16_t>(m_format);
        builder.swap_endianness();
        builder.seek_cur(sizeof(std::uint16_t));
        builder.write<std::uint16_t>(m_elementOffset);
        builder.write<std::uint16_t>(m_bufferIndex);
    }
}
} // namespace bfres