#include "bfres/skeleton/ResBone.h"

namespace bfres::skeleton {
void ResBone::deserialize(ResParser& parser)
{
    m_name = parser.parse_string_offset();

    if (!parser.is_wiiu()) {
        m_userData = parser.parse_offset_and_dict<ResUserData>();
        if (parser.version_major2() >= 9)
            parser.seek_cur(sizeof(std::uint64_t));
        else if (parser.version_major2() == 8)
            parser.seek_cur(sizeof(std::uint64_t) * 2);
    }

    m_index = parser.read<std::uint16_t>();
    m_parentIndex = parser.read<std::uint16_t>();
    m_smoothMtxIndex = parser.read<std::int16_t>();
    m_rigidMtxIndex = parser.read<std::int16_t>();
    m_billboardIndex = parser.read<std::uint16_t>();

    auto num_user_data {parser.read<std::uint16_t>()};

    m_flags = parser.read<std::uint32_t>();
    m_scale = parser.read<Vector3f>();
    m_rotation = parser.read<Vector4f>();
    m_translation = parser.read<Vector3f>();

    if (parser.is_wiiu()) {
        m_userData = parser.parse_dict_offset<ResUserData>();

        if (parser.version() < 0x03040000)
            m_invModelMtx = parser.read<Matrix34f>();
    }
}

void ResBone::serialize(ResBuilder& builder, int index)
{
    builder.queue_string_table_entry(m_name);

    if (!builder.is_wiiu()) {
        // TODO: Write switch data
    }

    builder.write<std::uint16_t>(index);
    builder.write<std::uint16_t>(m_parentIndex);
    builder.write<std::uint16_t>(m_smoothMtxIndex);
    builder.write<std::uint16_t>(m_rigidMtxIndex);
    builder.write<std::uint16_t>(m_billboardIndex);

    builder.write_count(m_userData.size());

    builder.write<std::uint32_t>(m_flags);
    builder.write<Vector3f>(m_scale);
    builder.write<Vector4f>(m_rotation);
    builder.write<Vector3f>(m_translation);

    if (builder.is_wiiu()) {
        builder.queue_offset32(m_userData);

        if (builder.version() < 0x03040000)
            builder.write<Matrix34f>(m_invModelMtx);
    }

    // TODO: deque user_data_dict
}
} // namespace bfres