#include "bfres/skeleton/ResSkeleton.h"

#include "bfres/common/ResCommon.h"

namespace bfres::skeleton {
void ResSkeleton::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header = parser.read<BinaryBlockHeader>();

        m_flags = parser.read<std::uint32_t>();

        auto num_bone {parser.read<std::uint16_t>()};
        auto num_matrix_to_bone {parser.read<std::uint16_t>()};
        auto num_rigid_matrix {parser.read<std::uint16_t>()};

        parser.seek(2, std::ios_base::cur);

        m_bones = parser.parse_dict_offset<ResBone>();

        // Offset to bone array
        parser.seek(sizeof(std::uint32_t), std::ios_base::cur);

        m_matrixToBoneTable =
            parser.parse_vector<std::uint16_t>(num_matrix_to_bone);
        if (parser.version() >= 0x03040000)
            m_inverseModelMatrices =
                parser.parse_vector<Matrix34f>(num_matrix_to_bone);

        auto user_ptr {parser.read<std::uint32_t>()};
    }
    else {
        if (parser.version_major2() >= 9) {
            auto signature {parser.read<std::uint32_t>()};
            m_flags = parser.read<std::uint32_t>();
        }
        else {
            auto header {parser.read<BinaryBlockHeader64>()};
        }

        auto to_bone_dict {parser.read_offset64()};
        auto to_bones {parser.read_offset64()};
        m_bones = parser.parse_dict_offset<ResBone>(to_bones, to_bone_dict);

        auto to_matrix_to_bone_table {parser.read_offset64()};
        auto to_inverse_model_matrices {parser.read_offset64()};

        if (parser.version_major2() == 8)
            parser.seek_cur(sizeof(std::uint64_t) * 2);
        else
            parser.seek_cur(sizeof(std::uint64_t));

        // auto user_ptr {parser.read_offset64()};
        if (parser.version_major2() < 9)
            m_flags = parser.read<std::uint32_t>();

        auto num_bones {parser.read_count()};
        auto num_smooth_matrix {parser.read_count()};
        auto num_rigid_matrix {parser.read_count()};
        parser.seek_cur(sizeof(std::uint32_t) + 2);

        m_matrixToBoneTable = parser.parse_vector<std::uint16_t>(
            num_smooth_matrix + num_rigid_matrix, to_matrix_to_bone_table);
        m_inverseModelMatrices = parser.parse_vector<Matrix34f>(
            num_smooth_matrix, to_inverse_model_matrices);
    }
}

void ResSkeleton::serialize(ResBuilder& builder)
{
    builder.write_string("FSKL");
    if (builder.is_wiiu()) {
        // Header
        builder.write<std::uint32_t>(m_flags);

        builder.write_count(m_bones.size());
        builder.write_count(m_matrixToBoneTable.size());
        builder.write_count(m_inverseModelMatrices.size());

        builder.seek_cur(sizeof(std::uint16_t));

        builder.queue_offset32(m_bones);
        auto bones_offset_pos {builder.tell()};
        builder.write_offset32(0);

        builder.queue_offset32(m_matrixToBoneTable);
        if (builder.version() >= 0x03040000)
            builder.queue_offset32(m_inverseModelMatrices);

        // User Pointer, set at runtime
        builder.write_offset32(0);

        std::size_t bones_pos {builder.tell()};
        // Bones
        for (int i {0}; i < m_bones.size(); ++i) {
            builder.queue_value(m_bones[i]);
            m_bones[i].serialize(builder, i);
        }

        // Bones Dict
        builder.build_dict_and_offset(m_bones);

        builder.set_offset_pos(&m_bones, bones_offset_pos);
        builder.set_value_pos(&m_bones, bones_pos);
        builder.save_offset(m_bones);

        for (auto& bone : m_bones)
            builder.save_offset(bone);
    }
}
} // namespace bfres