#include "bfres/materialanim/ParamAnimInfo.h"

namespace bfres::materialanim {
void ParamAnimInfo::deserialize(ResParser& parser)
{
    if (!parser.is_wiiu())
        m_name = parser.parse_string_offset();

    m_curveBeginIndex = parser.read<std::uint16_t>();
    auto num_float_curve {parser.read<std::uint16_t>()};
    auto num_int_curve {parser.read<std::uint16_t>()};
    m_constantBeginIndex = parser.read<std::uint16_t>();
    auto num_constant {parser.read<std::uint16_t>()};
    m_subBindIndex = parser.read<std::uint16_t>();

    if (parser.is_wiiu())
        m_name = parser.parse_string_offset();
    else
        parser.seek_cur(sizeof(std::uint32_t));
}

void ParamAnimInfo::serialize(ResBuilder& builder)
{
    if (!builder.is_wiiu())
        builder.queue_string_table_entry(m_name);

    builder.write<std::uint16_t>(m_curveBeginIndex);
    builder.write<std::uint16_t>(0);
    builder.write<std::uint16_t>(0);
    builder.write<std::uint16_t>(m_constantBeginIndex);
    builder.write<std::uint16_t>(0);
    builder.write<std::uint16_t>(m_subBindIndex);

    if (builder.is_wiiu())
        builder.queue_string_table_entry(m_name);
    else
        builder.seek_cur(sizeof(std::uint32_t));
}
}