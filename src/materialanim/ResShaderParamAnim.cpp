#include "bfres/materialanim/ResShaderParamAnim.h"

namespace bfres::materialanim {
void ResShaderParamMatAnim::deserialize(ResParser& parser)
{
    auto num_param_anim {parser.read<std::uint16_t>()};
    auto num_curves {parser.read<std::uint16_t>()};
    auto num_constants {parser.read<std::uint16_t>()};

    parser.seek_cur(sizeof(std::uint16_t));

    m_curveBeginIndex = parser.read<std::int32_t>();
    m_paramAnimBeginIndex = parser.read<std::int32_t>();

    m_name = parser.parse_string_offset();
    m_paramAnimInfos = parser.parse_vector<ParamAnimInfo>(num_param_anim);
    m_curves = parser.parse_vector<ResAnimCurve>(num_curves);
    m_animationConstants = parser.parse_vector<AnimationConstant>(num_constants);
}

void ResShaderParamMatAnim::serialize(ResBuilder& builder)
{
    builder.write_count(m_paramAnimInfos.size());
    builder.write_count(m_curves.size());
    builder.write_count(m_animationConstants.size());

    builder.seek_cur(sizeof(std::uint16_t));

    builder.write<std::int32_t>(m_curveBeginIndex);
    builder.write<std::int32_t>(m_paramAnimBeginIndex);

    builder.queue_string_table_entry(m_name);

    builder.queue_offset32(m_paramAnimInfos);
    builder.queue_offset32(m_curves);
    builder.queue_offset32(m_animationConstants);
}

void ResShaderParamAnim::deserialize(ResParser& parser)
{
    auto header = parser.read<BinaryBlockHeader>();

    m_name = parser.parse_string_offset();
    m_path = parser.parse_string_offset();

    m_flags = parser.read<std::uint32_t>();

    auto num_frame {parser.read<std::int32_t>()};
    auto num_material_anim {parser.read<std::uint16_t>()};
    auto num_user_data {parser.read<std::uint16_t>()};
    auto num_param_anim {parser.read<std::int32_t>()};
    auto num_curve {parser.read<std::int32_t>()};

    m_bakedSize = parser.read<std::uint32_t>();

    m_bindModel = parser.parse_offset<model::ResModel>();
    m_bindIndices = parser.parse_vector<std::uint16_t>(num_material_anim);
    m_materialAnims = parser.parse_vector<ResShaderParamMatAnim>(num_material_anim);
    m_userData = parser.parse_dict_offset<ResUserData>();
}

void ResShaderParamAnim::serialize(ResBuilder& builder)
{
    builder.write_string("FSHU");

    builder.queue_string_table_entry(m_name);
    builder.queue_string_table_entry(m_path);

    builder.write<std::uint32_t>(m_flags);

    int num_frames {0};
    int num_param_anim {0};
    int num_curves {0};
    for (auto& mat_anim : m_materialAnims) {
        num_param_anim += mat_anim.anim_infos().size();
        num_curves += mat_anim.curves().size();
        for (auto& curve : mat_anim.curves())
            num_frames += curve.frames().size();
    }

    builder.write<std::int32_t>(num_frames);
    builder.write_count(m_materialAnims.size());
    builder.write_count(m_userData.size());
    builder.write<std::int32_t>(num_param_anim);
    builder.write<std::int32_t>(num_curves);

    builder.write<std::uint32_t>(m_bakedSize);

    builder.queue_offset32(m_bindModel);
    builder.queue_offset32(m_bindIndices);
    builder.queue_offset32(m_materialAnims);
    builder.queue_offset32(m_userData);
}
} // namespace bfres