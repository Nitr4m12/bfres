#include "bfres/materialanim/ResTexturePatternAnim.h"

namespace bfres::materialanim {
void ResTexturePatternMatAnim::deserialize(ResParser& parser)
{
    auto num_pattern_anim {parser.read<std::uint16_t>()};
    auto num_curve {parser.read<std::uint16_t>()};

    m_curveBeginIndex = parser.read<std::int32_t>();
    m_patternAnimBeginIndex = parser.read<std::int32_t>();

    m_name = parser.parse_string_offset();

    m_patternAnimInfos = parser.parse_vector<PatternAnimInfo>(num_pattern_anim);
    m_curves = parser.parse_vector<ResAnimCurve>(num_curve);
    m_baseValues = parser.parse_vector<std::uint16_t>(num_pattern_anim);
}

void ResTexturePatternMatAnim::serialize(ResBuilder& builder) {}

void ResTexturePatternAnim::deserialize(ResParser& parser)
{
    auto header {parser.read<BinaryBlockHeader>()};

    m_name = parser.parse_string_offset();
    m_path = parser.parse_string_offset();

    m_flags = parser.read<std::uint16_t>();

    auto num_user_data {parser.read<std::int16_t>()};
    auto num_frame {parser.read<std::int32_t>()};
    auto num_texture_ref {parser.read<std::uint16_t>()};
    auto num_material_anim {parser.read<std::uint16_t>()};
    auto num_pattern_anim {parser.read<std::uint32_t>()};
    auto num_curve {parser.read<std::uint32_t>()};
    m_bakedSize = parser.read<std::uint32_t>();

    m_bindModel = parser.parse_offset<model::ResModel>();
    m_bindIndices = parser.parse_vector<std::uint16_t>(num_material_anim);
    m_materialAnims =
        parser.parse_vector<ResTexturePatternMatAnim>(num_material_anim);
    m_textureRefs = parser.parse_dict_offset<texture::ResTextureRef>();
    m_userData = parser.parse_dict_offset<ResUserData>();
}

void ResTexturePatternAnim::serialize(ResBuilder& builder) {}
} // namespace bfres