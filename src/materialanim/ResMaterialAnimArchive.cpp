#include "bfres/materialanim/ResMaterialAnimArchive.h"

namespace bfres::materialanim {
void ResMaterialAnim::deserialize(ResParser& parser)
{
    m_name = parser.parse_string_offset();

    auto to_shader_param_anims {parser.read<std::uint64_t>()};
    auto to_texture_pattern_anims {parser.read<std::uint64_t>()};
    auto to_curves {parser.read<std::uint64_t>()};
    auto to_anim_constants {parser.read<std::uint64_t>()};

    auto shader_param_curve_index {parser.read<std::uint16_t>()};
    auto texture_pattern_curve_index {parser.read<std::uint16_t>()};
    auto visual_constant_index {parser.read<std::uint16_t>()};
    auto visual_curve_index {parser.read<std::uint16_t>()};
    auto first_visual_constant_index {parser.read<std::uint16_t>()};

    auto num_shader_param_anims {parser.read<std::uint16_t>()};
    auto num_texture_pattern_anims {parser.read<std::uint16_t>()};
    auto num_anim_constant {parser.read<std::uint16_t>()};
    auto num_curves {parser.read<std::uint16_t>()};
    parser.seek(6, std::ios_base::cur);

    m_curves = parser.parse_vector<ResAnimCurve>(num_curves, to_curves);
    m_paramAnimInfos = parser.parse_vector<ParamAnimInfo>(num_shader_param_anims, to_shader_param_anims);
    m_patternAnimInfos = parser.parse_vector<PatternAnimInfo>(
        num_texture_pattern_anims, to_texture_pattern_anims);
    m_constants = parser.parse_vector<AnimationConstant>(num_anim_constant,
                                                         to_anim_constants);

    // TODO: Populate the base values array for texture pattern anims
    // for (int i {0}; i < m_patternAnimInfos.size(); ++i) {
    //     if (m_patternAnimInfos[i].begin)
    // }
}

// void ResMaterialAnim::serialize(ResBuilder& builder)
// {
//     builder.write_string_offset(m_name);

//     builder.write_offset64(0);
//     builder.write_offset64(0);
//     builder.write_offset64(0);
//     builder.write_offset64(0);

//     builder.write<std::uint16_t>(0);
//     builder.write<std::uint16_t>(0);
//     builder.write<std::uint16_t>(0);
//     builder.write<std::uint16_t>(0);
//     builder.write<std::uint16_t>(0);

//     builder.write_count(m_paramAnimInfos.size());
//     builder.write_count(m_patternAnimInfos.size());
//     builder.write_count(m_constants.size());
//     builder.write_count(m_curves.size());
//     builder.seek_cur(sizeof(std::uint32_t) + 2);
// }

void ResMaterialAnimArchive::deserialize(ResParser& parser)
{
    if (parser.version_major2() >= 9) {
        auto signature = parser.read_string(4);
        m_flags = parser.read<std::uint16_t>();
        parser.seek(sizeof(std::uint16_t), std::ios_base::cur);
    }
    else
        parser.read<BinaryBlockHeader64>();

    m_name = parser.parse_string_offset();
    m_path = parser.parse_string_offset();
    m_bindModel = parser.parse_offset<model::ResModel>();

    auto to_bind_indices {parser.read<std::uint64_t>()};
    auto to_mat_anims {parser.read<std::uint64_t>()};
    parser.read<std::uint64_t>();
    auto to_texture_names = parser.read<std::uint64_t>();

    auto to_user_data = parser.read<std::uint64_t>();
    m_userData = parser.parse_dict_offset<ResUserData>(to_user_data);

    auto to_texture_binds {parser.read<std::uint64_t>()};

    if (parser.version_major2() < 9)
        m_flags = parser.read<std::uint16_t>();

    std::int32_t num_frames;

    if (parser.version_major2() >= 9) {
        num_frames = parser.read<std::int32_t>();
        m_bakedSize = parser.read<std::uint32_t>();
    }

    auto num_user_data = parser.read<std::uint16_t>();
    auto num_mat_anims = parser.read<std::uint16_t>();
    auto num_curves = parser.read<std::uint16_t>();

    if (parser.version_major2() < 9) {
        num_frames = parser.read<std::int32_t>();
        m_bakedSize = parser.read<std::uint32_t>();
    }

    auto num_shader_param_anim {parser.read<std::uint16_t>()};
    auto num_texture_pattern_anim {parser.read<std::uint16_t>()};
    auto num_bone_vis_anim {parser.read<std::uint16_t>()};
    auto num_textures {parser.read<std::uint16_t>()};

    if (parser.version_major2() >= 9)
        parser.seek_cur(sizeof(std::uint16_t));

    m_bindIndices =
        parser.parse_vector<std::uint16_t>(num_mat_anims, to_bind_indices);

    auto texture_names {
        parser.parse_vector_of_strings(num_textures, to_texture_names)};
    m_textureRefs.resize(num_textures);
    for (int i {0}; i < m_textureRefs.size(); ++i)
        m_textureRefs[i].name = texture_names[i];

    m_textureBinds =
        parser.parse_vector<std::int64_t>(num_textures, to_texture_binds);
    m_materialAnims =
        parser.parse_vector<ResMaterialAnim>(num_mat_anims, to_mat_anims);
}

// void ResMaterialAnimArchive::serialize(ResBuilder &builder)
// {
//     builder.write_string("FMAA");
//     if (builder.version_major2() >= 9) {
//         builder.write<std::uint16_t>(m_flags);
//         builder.seek_cur(sizeof(std::uint16_t));
//     }
//     else {
//         builder.write<std::uint32_t>(0);
//         builder.write<std::uint32_t>(0);
//         builder.seek_cur(sizeof(std::uint32_t));
//     }

//     builder.write_string_offset(m_name);
//     builder.write_string_offset(m_path);

//     builder.write_value_offset(m_bindModel);
//     builder.write_offset64(0);
//     builder.write_offset64(0);
//     builder.write_offset64(0);
//     builder.write_offset64(0);

//     builder.write_offset_and_dict(m_userData);

//     builder.write_offset64(0);

//     if (builder.version_major2() < 9)
//         builder.write<std::uint16_t>(m_flags);

//     if (builder.version_major2() >= 9) {
//         builder.write<std::int32_t>(0);
//         builder.write<std::uint32_t>(m_bakedSize);
//     }

//     builder.write_count(0);
//     builder.write_count(0);
//     builder.write_count(0);
//     builder.write_count(0);

//     if (builder.version_major2() >= 9)
//         builder.seek_cur(sizeof(std::uint16_t));

// }
} // namespace bfres
