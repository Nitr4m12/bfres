#include "bfres/materialanim/PatternAnimInfo.h"

namespace bfres::materialanim {
void PatternAnimInfo::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        m_curveIndex = parser.read<std::int8_t>();
        m_subbindIndex = parser.read<std::int8_t>();
        parser.seek_cur(sizeof(std::uint16_t));
        m_name = parser.parse_string_offset();
    }
    else {
        m_name = parser.parse_string_offset();
        m_curveIndex = parser.read<std::int16_t>();
        m_beginConstantIndex = parser.read<std::uint16_t>();
        m_subbindIndex = parser.read<std::int8_t>();
        parser.seek_cur(sizeof(std::uint16_t)+1);
    }
}

void PatternAnimInfo::serialize(ResBuilder& builder)
{
    if (builder.is_wiiu()) {
        builder.write<std::int8_t>(m_curveIndex);
        builder.write<std::int8_t>(m_subbindIndex);
        builder.seek_cur(sizeof(std::uint16_t));
        builder.queue_string_table_entry(m_name);
    }
    else {
        builder.queue_string_table_entry(m_name);
        builder.write<std::int16_t>(m_curveIndex);
        builder.write<std::uint16_t>(m_beginConstantIndex);
        builder.write<std::int8_t>(m_subbindIndex);
        builder.seek_cur(sizeof(std::uint16_t)+1);
    }
}
} // namespace bfres