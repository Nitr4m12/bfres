#include "bfres/model/ResModel.h"

namespace bfres::model {
void ResModel::deserialize(ResParser& parser)
{
    if (parser.is_wiiu()) {
        auto header = parser.read<BinaryBlockHeader>();

        m_name = parser.parse_string_offset();
        m_path = parser.parse_string_offset();

        m_skeleton = parser.parse_offset<skeleton::ResSkeleton>();
        auto vertices_offset {parser.tell() + parser.read<std::int32_t>()};

        m_shapes = parser.parse_dict_offset<shape::ResShape>();
        m_materials = parser.parse_dict_offset<material::ResMaterial>();
        m_userData = parser.parse_dict_offset<ResUserData>();

        auto num_vertices {parser.read<std::uint16_t>()};
        auto num_shape {parser.read<std::uint16_t>()};
        auto num_material {parser.read<std::uint16_t>()};
        auto num_user_data {parser.read<std::uint16_t>()};

        m_totalProcessVertex = parser.read<std::uint32_t>();
        auto user_ptr {parser.read<std::int32_t>()};

        m_vertices.resize(num_vertices);
        std::size_t og_pos {parser.tell()};
        parser.seek(vertices_offset);

        for (int i {0}; i < num_vertices; ++i)
            m_vertices[i].deserialize(parser);

        parser.seek(og_pos);
    }
    else {
        BinaryBlockHeader64 header;
        if (parser.version_major2() >= 9) {
            auto signature {parser.read<std::uint32_t>()};
            m_flags = parser.read<std::uint32_t>();
        }
        else
            header = parser.read<BinaryBlockHeader64>();

        m_name = parser.parse_string_offset();
        m_path = parser.parse_string_offset();
        m_skeleton = parser.parse_offset<skeleton::ResSkeleton>();
        auto vertices_offset {parser.read<std::uint64_t>()};

        auto shapes_offset {parser.read<std::uint64_t>()};
        m_shapes = parser.parse_dict_offset<shape::ResShape>(shapes_offset);

        auto materials_offset {parser.read<std::uint64_t>()};
        std::uint64_t material_dict_offset;
        if (parser.version_major2() >= 9) {
            parser.seek_cur(sizeof(std::uint64_t));
            material_dict_offset = parser.read<std::uint64_t>();
        }
        else {
            material_dict_offset = parser.read<std::uint64_t>();
            parser.seek_cur(sizeof(std::uint64_t));
        }

        m_materials = parser.parse_dict_offset<material::ResMaterial>(
            materials_offset, material_dict_offset);

        if (parser.version_major2() >= 10)
            parser.seek_cur(sizeof(std::uint64_t));

        auto user_data_offset {parser.read<std::uint64_t>()};
        m_userData = parser.parse_dict_offset<ResUserData>(user_data_offset);

        // auto user_ptr {parser.read<std::uint64_t>()};

        auto vertex_count {parser.read<std::uint16_t>()};
        m_vertices =
            parser.parse_vector<vertex::ResVertex>(vertex_count, vertices_offset);

        auto shape_count {parser.read<std::uint16_t>()};
        auto material_count {parser.read<std::uint16_t>()};

        std::uint16_t user_data_count;
        if (parser.version_major2() >= 9) {
            parser.seek_cur(sizeof(std::uint16_t));
            user_data_count = parser.read<std::uint16_t>();
            parser.seek_cur(sizeof(std::uint16_t));
        }
        else {
            user_data_count = parser.read<std::uint16_t>();

            // totalVertexCount, not used in lib
            parser.seek_cur(sizeof(std::uint32_t));
        }
        parser.seek_cur(sizeof(std::uint32_t));
    }
}

void ResModel::serialize(ResBuilder& builder)
{
    builder.write_string("FMDL");
    if (builder.is_wiiu()) {
        builder.queue_string_table_entry(m_name);
        builder.queue_string_table_entry(m_path);

        builder.queue_offset32(m_skeleton);
        builder.queue_offset32(m_vertices);
        builder.queue_offset32(m_shapes);
        builder.queue_offset32(m_materials);
        builder.queue_offset32(m_userData);

        builder.write_count(m_vertices.size());
        builder.write_count(m_shapes.size());
        builder.write_count(m_materials.size());
        builder.write_count(m_userData.size());

        builder.write<std::uint32_t>(m_totalProcessVertex);

        // User Pointer, set at runtime
        builder.write_offset32(0);

        builder.queue_value(m_vertices);
        for (int i{0}; i<m_vertices.size(); ++i)
            m_vertices[i].serialize(builder, i);
        builder.save_offset(m_vertices);

        builder.build_value_and_offset(m_skeleton);

        // Dicts are stored after the skeleton
        builder.build_dict_and_offset(m_shapes);
        builder.build_dict_and_offset(m_materials);
        builder.build_dict_and_offset(m_userData);

        // The rest are stored after the Dicts
        builder.build_values_and_offsets(m_shapes);

        for (auto& vertex : m_vertices) {
            builder.queue_value(vertex.vtx_attribs());
            builder.save_offset(vertex.vtx_attribs());
            builder.build_values(vertex.vtx_attribs());
            builder.build_value_and_offset(vertex.vtx_buffer());

            // Attributes dict
            builder.queue_value(vertex.vtx_attribs_dict_dummy());
            builder.save_offset(vertex.vtx_attribs_dict_dummy());
            builder.build_dict(vertex.vtx_attribs());
            builder.save_offsets(vertex.vtx_attribs());
        }

        builder.build_values_and_offsets(m_materials);
        builder.build_values_and_offsets(m_userData);


    }
}
} // namespace bfres