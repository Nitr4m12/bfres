#include "bfres/common/IResData.h"

#ifndef BFRES_SHAPEANIM_RESSHAPEANIMKEYINFO_H
#define BFRES_SHAPEANIM_RESSHAPEANIMKEYINFO_H

namespace bfres::shapeanim {
class ResShapeAnimKeyInfo : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::uint8_t m_curveIndex;
    std::uint8_t m_subbindIndex;
    std::string m_name;
};
}

#endif