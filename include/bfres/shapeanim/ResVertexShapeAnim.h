#include "bfres/common/ResAnimCurve.h"
#include "bfres/shapeanim/ResShapeAnimKeyInfo.h"

#ifndef BFRES_SHAPEANIM_RESVERTEXSHAPEANIM_H
#define BFRES_SHAPEANIM_RESVERTEXSHAPEANIM_H

namespace bfres::shapeanim {
class ResVertexShapeAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::int32_t m_firstCurveIndex;
    std::int32_t m_firstKeyShapeAnimIndex;
    std::string m_name;

    std::vector<ResShapeAnimKeyInfo> m_shapeAnimKeyInfos;
    std::vector<ResAnimCurve> m_curves;
    std::vector<float> m_baseTransformValues;
};
} // namespace bfres::shapeanim

#endif