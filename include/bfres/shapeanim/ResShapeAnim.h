#include "bfres/common/IResData.h"

#include "bfres/model/ResModel.h"
#include "bfres/shapeanim/ResVertexShapeAnim.h"

#ifndef BFRES_SHAPEANIM_RESHAPEANIM_H
#define BFRES_SHAPEANIM_RESHAPEANIM_H

namespace bfres::shapeanim {
class ResShapeAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    // void serialize(ResBuilder& builder) override;

private:
    std::string name;
    std::string path;

    std::uint16_t flags;
    std::int16_t numFrame;
    std::uint32_t bakedSize;

    model::ResModel bindModel;
    std::vector<std::uint16_t> bindIndices;
    std::vector<ResVertexShapeAnim> vertexShapeAnims;
    std::vector<ResUserData> userData;
};
} // namespace bfres

#endif