#include "bfres/common/ResCommon.h"

#include "bfres/material/ResMaterial.h"
#include "bfres/shape/ResShape.h"
#include "bfres/skeleton/ResSkeleton.h"
#include "bfres/common/ResUserData.h"

#ifndef BFRES_RESMODEL_H
#define BFRES_RESMODEL_H

namespace bfres::model {
class ResModel : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::uint32_t m_flags;

    std::string m_name;
    std::string m_path;

    skeleton::ResSkeleton m_skeleton;
    std::vector<vertex::ResVertex> m_vertices;
    std::vector<shape::ResShape> m_shapes;
    std::vector<material::ResMaterial> m_materials;
    std::vector<ResUserData> m_userData;

    std::uint32_t m_totalProcessVertex;
};
} // namespace bfres

#endif