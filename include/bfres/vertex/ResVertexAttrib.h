#include "gx2/enum.h"

#include "bfres/common/IResData.h"

#ifndef BFRES_SHAPE_RESVERTEXATTRIB_H
#define BFRES_SHAPE_RESVERTEXATTRIB_H

namespace bfres::vertex {
class ResVertexAttrib : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::uint8_t m_bufferIndex;
    std::uint16_t m_elementOffset;
    GX2AttribFormat m_format;
};
} // namespace bfres

#endif