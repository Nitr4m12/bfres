#include "bfres/common/IResData.h"

#include "bfres/vertex/ResVertexAttrib.h"
#include "bfres/vertex/ResVertexBuffer.h"

#ifndef BFRES_SHAPE_RESVERTEX_H
#define BFRES_SHAPE_RESVERTEX_H

namespace bfres::vertex {
class ResVertex : public IResData {
public:
    void deserialize(ResParser& parser) override;
    // void serialize(ResBuilder& builder) override;
    void serialize(ResBuilder& builder, int index);

    auto& vtx_attribs() { return m_vtxAttribs; }
    auto& vtx_attribs_dict_dummy() { return m_vtxAttribsDictDummy; }
    auto& vtx_buffer() { return m_vtxBuffer; }

private:
    std::uint8_t m_vtxSkinCount;

    std::vector<ResVertexAttrib> m_vtxAttribs;
    std::int32_t m_vtxAttribsDictDummy;

    std::vector<ResVertexBuffer> m_vtxBuffer;
};
} // namespace bfres

#endif