#include "bfres/common/IResData.h"
#include "bfres/common/ResBuffer.h"

#ifndef BFRES_SHAPE_RESVERTEXBUFFER_H
#define BFRES_SHAPE_RESVERTEXBUFFER_H

namespace bfres::vertex {
class ResVertexBuffer : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    ResBuffer m_gfxBuffer;
    std::vector<std::uint8_t> m_data;
};
} // namespace bfres

#endif