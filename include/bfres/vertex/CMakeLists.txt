target_sources(bfres PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/ResVertex.h
    ${CMAKE_CURRENT_SOURCE_DIR}/ResVertexAttrib.h
    ${CMAKE_CURRENT_SOURCE_DIR}/ResVertexBuffer.h
)