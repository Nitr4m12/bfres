#include <cstdint>

#include <binaryio/type_utils.h>

#ifndef BFRES_MATERIALANIM_ANIMATIONCONSTANT_H
#define BFRES_MATERIALANIM_ANIMATIONCONSTANT_H

namespace bfres::materialanim {
struct AnimationConstant {
    std::uint32_t targetOffset;
    float value;

    BINARYIO_DEFINE_FIELDS(AnimationConstant, targetOffset, value);
};
} // namespace bfres

#endif