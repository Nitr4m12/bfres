#include "bfres/common/IResData.h"

#ifndef BFRES_MATERIALANIM_PATTERNANIMINFO_H
#define BFRES_MATERIALANIM_PATTERNANIMINFO_H

namespace bfres::materialanim {
class PatternAnimInfo : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::int8_t m_curveIndex;
    std::int8_t m_subbindIndex;
    std::uint16_t m_beginConstantIndex;
    std::string m_name;
};
} // namespace bfres

#endif