#include "bfres/common/ResAnimCurve.h"
#include "bfres/model/ResModel.h"

#include "bfres/materialanim/ResMaterialAnim.h"
#include "bfres/materialanim/AnimationConstant.h"
#include "bfres/materialanim/ParamAnimInfo.h"
#include "bfres/materialanim/PatternAnimInfo.h"

#ifndef BFRES_MATERIALANIM_RESMATERIALANIMARCHIVE_H
#define BFRES_MATERIALANIM_RESMATERIALANIMARCHIVE_H

namespace bfres::materialanim {
struct TextureRef {
    std::string name;
    std::uint64_t toTexture;

    BINARYIO_DEFINE_FIELDS(TextureRef, name, toTexture);
};

class ResMaterialAnimArchive : public IResData {
public:
    void deserialize(ResParser& parser) override;
    // void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::string m_path;

    std::uint16_t m_flags;
    std::uint32_t m_bakedSize;

    model::ResModel m_bindModel;
    std::vector<std::uint16_t> m_bindIndices;
    std::vector<ResUserData> m_userData;
    std::vector<std::int64_t> m_textureBinds;
    std::vector<TextureRef> m_textureRefs;
    std::vector<ResMaterialAnim> m_materialAnims;
};
} // namespace bfres::materialanim

#endif