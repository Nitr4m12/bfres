#include "bfres/common/IResData.h"

#ifndef BFRES_MATERIALANIM_PARAMANIMINFO_H
#define BFRES_MATERIALANIM_PARAMANIMINFO_H

namespace bfres::materialanim {
class ParamAnimInfo : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;

    std::uint16_t m_curveBeginIndex;
    std::uint16_t m_constantBeginIndex;
    std::uint16_t m_subBindIndex;

};
} // namespace bfres

#endif