#include "bfres/common/ResAnimCurve.h"

#include "bfres/materialanim/AnimationConstant.h"
#include "bfres/materialanim/ParamAnimInfo.h"
#include "bfres/materialanim/PatternAnimInfo.h"

#ifndef BFRES_MATERIALANIM_RESMATERIALANIM_H
#define BFRES_MATERIALANIM_RESMATERIALANIM_H

namespace bfres::materialanim {
class ResMaterialAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    // void serialize(ResBuilder& builder) override;

private:
    std::string m_name;

    std::vector<ResAnimCurve> m_curves;
    std::vector<ParamAnimInfo> m_paramAnimInfos;
    std::vector<PatternAnimInfo> m_patternAnimInfos;
    std::vector<AnimationConstant> m_constants;
    std::vector<std::uint16_t> m_baseValues;
};
} // namespace bfres::materialanim

#endif