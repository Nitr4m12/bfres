#include "bfres/common/ResAnimCurve.h"

#include "bfres/model/ResModel.h"
#include "bfres/texture/ResTextureRef.h"
#include "bfres/materialanim/PatternAnimInfo.h"

#ifndef BFRES_MATERIALANIM_RESTEXTUREPATTERNANIM_H
#define BFRES_MATERIALANIM_RESTEXTUREPATTERNANIM_H

namespace bfres::materialanim {
class ResTexturePatternMatAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::int32_t m_curveBeginIndex;
    std::int32_t m_patternAnimBeginIndex;

    std::string m_name;
    std::vector<PatternAnimInfo> m_patternAnimInfos;
    std::vector<ResAnimCurve> m_curves;
    std::vector<std::uint16_t> m_baseValues;
};

class ResTexturePatternAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::string m_path;

    std::uint16_t m_flags;
    std::uint32_t m_bakedSize;

    model::ResModel m_bindModel;
    std::vector<std::uint16_t> m_bindIndices;
    std::vector<ResTexturePatternMatAnim> m_materialAnims;
    std::vector<texture::ResTextureRef> m_textureRefs;
    std::vector<ResUserData> m_userData;
};

} // namespace bfres::materialanim

#endif