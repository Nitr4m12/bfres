#include "bfres/common/ResAnimCurve.h"
#include "bfres/common/ResDictionary.h"

#include "bfres/model/ResModel.h"

#include "bfres/materialanim/AnimationConstant.h"
#include "bfres/materialanim/ParamAnimInfo.h"

#ifndef BFRES_MATERIALANIM_RESSHADERPARAMANIM_H
#define BFRES_MATERIALANIM_RESSHADERPARAMANIM_H

namespace bfres::materialanim {
class ResShaderParamMatAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

    auto curves() { return m_curves; }

    auto anim_infos() { return m_paramAnimInfos; }

private:
    std::int32_t m_curveBeginIndex;
    std::int32_t m_paramAnimBeginIndex;

    std::string m_name;
    std::vector<ParamAnimInfo> m_paramAnimInfos;
    std::vector<ResAnimCurve> m_curves;
    std::vector<AnimationConstant> m_animationConstants;
};

class ResShaderParamAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::string m_path;

    std::uint32_t m_flags;
    std::uint32_t m_bakedSize;

    model::ResModel m_bindModel;
    std::vector<std::uint16_t> m_bindIndices;
    std::vector<ResShaderParamMatAnim> m_materialAnims;
    std::vector<ResUserData> m_userData;
};
} // namespace bfres::materialanim

#endif