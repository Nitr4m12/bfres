#include <cstdint>

#include "bfres/material/TextureSrt.h"

#ifndef BFRES_MATERIAL_TEXTURESRTEX_H
#define BFRES_MATERIAL_TEXTURESRTEX_H

namespace bfres::material {
struct TextureSrtEx {
    TextureSrt texSrt;
    std::uint32_t mtx34Pointer;

    BINARYIO_DEFINE_FIELDS(TextureSrtEx, texSrt, mtx34Pointer);
};
} // namespace bfres::material

#endif