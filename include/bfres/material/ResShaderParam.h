#include <string>
#include <variant>

#include "math/Matrix.h"

#include "bfres/common/IResData.h"
#include "bfres/material/Srt3d.h"
#include "bfres/material/TextureSrtEx.h"

#ifndef BFRES_MATERIAL_SHADERPARAM_RESSHADERPARAM_H
#define BFRES_MATERIAL_SHADERPARAM_RESSHADERPARAM_H

namespace bfres::material {
class ResShaderParam : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::uint8_t m_type;
    std::uint8_t m_dataSize;
    std::uint32_t m_conversionCallbackPtr;
    std::uint16_t m_dependedIndex;
    std::uint16_t m_dependIndex;
    std::string m_name;

    std::variant<bool, Vector2<bool>, Vector3<bool>, Vector4<bool>, int, Vector2<int>, Vector3<int>,
                 Vector4<int>, unsigned int, Vector2<unsigned int>, Vector3<unsigned int>,
                 Vector4<unsigned int>, float, Vector2<float>, Vector3<float>, Vector4<float>,
                 Matrix<float, 2, 2>, Matrix<float, 2, 3>, Matrix<float, 2, 4>, Matrix<float, 3, 2>,
                 Matrix<float, 3, 3>, Matrix<float, 3, 4>, Matrix<float, 4, 2>, Matrix<float, 4, 3>,
                 Matrix<float, 4, 4>, Srt2d, Srt3d, TextureSrt, TextureSrtEx>
        m_data;
};
} // namespace bfres::material

#endif