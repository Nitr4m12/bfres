#include "bfres/common/ResDictionary.h"
#include "bfres/texture/ResTextureRef.h"
#include "bfres/common/ResUserData.h"

#include "bfres/material/ResRenderInfo.h"
#include "bfres/material/ResRenderState.h"
#include "bfres/material/ResSampler.h"
#include "bfres/material/ResShaderAssign.h"
#include "bfres/material/ResShaderParam.h"

#ifndef BFRES_MATERIAL_RESMATERIAL_H
#define BFRES_MATERIAL_RESMATERIAL_H

namespace bfres::material {
class ResMaterial : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::uint32_t m_flags;

    std::vector<ResRenderInfo> m_renderInfos;
    ResRenderState m_renderState;
    ResShaderAssign m_shaderAssign;
    std::vector<texture::ResTextureRef> m_textureRefs;
    std::vector<ResSampler> m_samplers;
    std::vector<ResShaderParam> m_shaderParams;
    std::vector<ResUserData> m_userData;
    std::vector<std::uint8_t> m_volatileFlags;
};

} // namespace bfres::material

#endif