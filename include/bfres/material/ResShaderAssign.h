#include <array>
#include <string>
#include <vector>

#include "bfres/common/IResData.h"

#ifndef BFRES_MATERIAL_RESSHADERASSIGN_H
#define BFRES_MATERIAL_RESSHADERASSIGN_H

namespace bfres::material {
// struct tmp {
//     char val[3];
// };

class ResShaderAssign : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_shaderArchiveName;
    std::string m_shadingModelName;

    std::vector<std::array<char, 3>> m_attribAssigns;
    std::vector<std::array<char, 3>> m_samplerAssigns;
    std::vector<std::array<char, 3>> m_shaderOptions;
};
} // namespace bfres::material

#endif