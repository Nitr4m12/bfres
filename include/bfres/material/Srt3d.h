#include <binaryio/type_utils.h>

#include "math/Vector.h"

#ifndef BFRES_MATERIAL_SRT3D_H
#define BFRES_MATERIAL_SRT3D_H

namespace bfres::material {
struct Srt3d {
    Vector3f scale;
    Vector3f rotation;
    Vector3f translation;

    BINARYIO_DEFINE_FIELDS(Srt3d, scale, rotation, translation);
};
} // namespace bfres::material

#endif