#include <array>
#include <cstdint>

#include <binaryio/type_utils.h>

#ifndef BFRES_MATERIAL_GX2SAMPLER_H
#define BFRES_MATERIAL_GX2SAMPLER_H

namespace bfres::material {
struct GX2Sampler {
    std::array<std::uint32_t, 3> flags;
    std::uint32_t handle;

    BINARYIO_DEFINE_FIELDS(GX2Sampler, flags, handle);
};
} // namespace bfres::material

#endif