#include <cstdint>

#include <binaryio/type_utils.h>

#ifndef BFRES_MATERIAL_RESRENDERSTATE_H
#define BFRES_MATERIAL_RESRENDERSTATE_H

namespace bfres::material {
struct ResRenderState {
    std::uint32_t flags;
    std::uint32_t polygonCtrl;
    std::uint32_t depthControl;
    std::uint32_t alphaTest1;
    float alphaTest2;
    std::uint32_t colorCtrl;
    std::uint32_t blendCtrl[2];
    float blendColor[4];

    BINARYIO_DEFINE_FIELDS(ResRenderState, flags, polygonCtrl, depthControl, alphaTest1, alphaTest2,
                           colorCtrl, blendCtrl, blendColor)
};
} // namespace bfres::material

#endif