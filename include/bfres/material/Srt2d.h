#include <binaryio/type_utils.h>

#include "math/Vector.h"

#ifndef BFRES_MATERIAL_SRT2D_H
#define BFRES_MATERIAL_SRT2D_H

namespace bfres::material {
struct Srt2d {
    Vector2f scale;
    float rotation;
    Vector2f translation;

    BINARYIO_DEFINE_FIELDS(Srt2d, scale, rotation, translation);
};
} // namespace bfres::material

#endif