#include <binaryio/type_utils.h>

#include "bfres/material/Srt2d.h"

#ifndef BFRES_MATERIAL_TEXTURESRT_H
#define BFRES_MATERIAL_TEXTURESRT_H

namespace bfres::material {
struct TextureSrt {
    enum class Mode {
        MODE_MAYA = 0,
        MODE_3DSMAX,
        MODE_SOFTIMAGE,
        MODE_NUM,
    };

    Mode mode;
    Srt2d srt;

    BINARYIO_DEFINE_FIELDS(TextureSrt, mode, srt);
};
} // namespace bfres::material

#endif