#include <variant>

#include "bfres/common/ResCommon.h"

#ifndef BFRES_MATERIAL_RESRENDERINFO_H
#define BFRES_MATERIAL_RESRENDERINFO_H

namespace bfres::material {
class ResRenderInfo : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::vector<std::variant<Vector2f, Vector2i, std::string>> m_data;
    std::string m_varName;
};
} // namespace bfres::material

#endif