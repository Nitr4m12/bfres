#include <string>

#include "bfres/common/IResData.h"
#include "bfres/material/GX2Sampler.h"

#ifndef BFRES_MATERIAL_RESSAMPLER_H
#define BFRES_MATERIAL_RESSAMPLER_H

namespace bfres::material {
class ResSampler : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

    // TODO: Functions to parse flags as needed

private:
    GX2Sampler m_gfxSampler;
    std::string m_name;
};
} // namespace bfres::material

#endif