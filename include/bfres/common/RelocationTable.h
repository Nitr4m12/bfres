#include <array>
#include <cstdint>
#include <vector>

#ifndef BFRES_RELOCATIONTABLE_H
#define BFRES_RELOCATIONTABLE_H

namespace bfres {
struct RelocationTable {
    struct Section {
        std::uint64_t ptr;
        std::uint32_t position;
        std::uint32_t size;
        std::int32_t entryIndex;
        std::int32_t entryCount;
    };

    std::array<std::uint8_t, 4> signature;
    std::uint32_t position;
    std::int32_t sectionCount;
    std::vector<Section> sections;
};
} // namespace bfres

#endif