#include <cstdint>
#include <vector>

#ifndef BFRES_RESDICT_H
#define BFRES_RESDICT_H

namespace bfres {

template <typename T>
struct ResNode {
    std::uint32_t refBit;
    std::uint16_t leftIndex;
    std::uint16_t rightIndex;
    std::int32_t toName;
    std::int32_t toData;
};

template <typename T>
struct ResDict {
    std::uint32_t size;
    std::int32_t numNodes;
    std::vector<ResNode<T>> nodes;
};

} // namespace bfres

#endif