#include <cstdint>

#include <binaryio/type_utils.h>

#ifndef BFRES_RESBUFFER_H
#define BFRES_RESBUFFER_H

namespace bfres {
struct ResBuffer {
    std::uint32_t dataPtr;
    std::uint32_t size;
    std::uint32_t handle;
    std::uint16_t stride;
    std::uint16_t numBuffering;
    std::uint32_t contextPtr;

    BINARYIO_DEFINE_FIELDS(ResBuffer, dataPtr, size, handle, stride,
                           numBuffering, contextPtr);
};
} // namespace bfres

#endif