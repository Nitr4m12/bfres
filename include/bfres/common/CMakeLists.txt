target_sources(bfres PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/IResData.h
    ${CMAKE_CURRENT_SOURCE_DIR}/RelocationTable.h
    ${CMAKE_CURRENT_SOURCE_DIR}/ResAnimCurve.h
    ${CMAKE_CURRENT_SOURCE_DIR}/ResBuffer.h
    ${CMAKE_CURRENT_SOURCE_DIR}/ResBuilder.h
    ${CMAKE_CURRENT_SOURCE_DIR}/ResCommon.h
    ${CMAKE_CURRENT_SOURCE_DIR}/ResDictionary.h
    ${CMAKE_CURRENT_SOURCE_DIR}/ResParser.h
    ${CMAKE_CURRENT_SOURCE_DIR}/ResUserData.h
)