#include <cstdint>
#include <variant>

#include "bfres/common/ResCommon.h"
#include "bfres/common/IResData.h"

#ifndef BFRES_ANIMCURVE_H
#define BFRES_ANIMCURVE_H

namespace bfres {
enum class AnimFlag { CURVE_BAKED = 0x1 << 0, PLAYPOLICY_LOOP = 0x1 << 2, NOT_BOUND = 0xFFFF };

enum class AnimCurveFrameType : std::uint16_t {
    Float = 0,
    FixedPoint16,
    Byte,
};

enum class AnimCurveKeyType : std::uint16_t {
    Float = 0,
    Int16,
    SByte,
};

enum class AnimCurveType : std::uint16_t {
    Cubic = 0,
    Linear,
    BakedFloat,
    StepInt = 4,
    BakedInt,
    StepBool,
    BakedBool,
};

enum class WrapMode {
    Clamp = 0,
    Repeat,
    Mirror,
};

class ResAnimCurve : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

    auto frames() { return m_frames; }

private:
    static const std::uint16_t frameTypeMask {0b00000000'00000011};
    static const std::uint16_t keyTypeMask {0b00000000'00001100};
    static const std::uint16_t curveTypeMask {0b00000000'01110000};

    std::uint16_t m_flags;
    std::uint32_t m_targetOffset;
    float m_startFrame;
    float m_endFrame;

    float m_dataScale;
    float m_dataOffset;

    // Only in version >= 3.4.0.0
    float m_dataDelta;

    std::vector<std::variant<float, FixedPoint16, std::uint8_t>> m_frames;
    std::vector<std::vector<std::variant<float, std::int16_t, std::int8_t>>> m_keys;
    std::vector<bool> m_keyStepBoolData;

    AnimCurveType m_curveType {AnimCurveType::Cubic};
    AnimCurveFrameType m_frameType {AnimCurveFrameType::Float};
    AnimCurveKeyType m_keyType {AnimCurveKeyType::Float};

    int elements_per_key() const
    {
        switch (m_curveType) {
        case AnimCurveType::Cubic:
            return 4;
        case AnimCurveType::Linear:
            return 2;
        default:
            return 1;
        }
    }
};
} // namespace bfres

#endif