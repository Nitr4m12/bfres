#include "bfres/common/ResParser.h"
#include "bfres/common/ResBuilder.h"

#ifndef BFRES_IRESDATA_H
#define BFRES_IRESDATA_H

namespace bfres {
struct IResData {
    virtual void deserialize(ResParser& parser) = 0;
    // virtual void deserialize(std::string file_name)
    // {
    //     std::ifstream ifs {file_name};
    //     ResParser parser {ifs};
    //     deserialize(parser);
    // };

    virtual void serialize(ResBuilder& builder){};
    // virtual void serialize(std::string file_name) = 0;
};
} // namespace bfres

#endif