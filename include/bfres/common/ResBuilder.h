#include <cstdint>
#include <limits>
#include <type_traits>
#include <vector>

#include <binaryio/writer.h>

#include "bfres/common/ResDictionary.h"

#ifndef BFRES_RESBUILDER_H
#define BFRES_RESBUILDER_H

namespace bfres {
class IResData;

template <typename T>
concept is_container = requires(T a) {
                           a.size();
                           a.begin();
                           a.reserve(1);
                       };

class ResBuilder : public binaryio::BinaryWriter {
public:
    ResBuilder(std::ostream& os)
        : binaryio::BinaryWriter(os, binaryio::endian::native) {};

    void seek_cur(const std::size_t offset)
    {
        this->seek(offset, std::ios_base::cur);
    }

    void write_offset32(const std::int32_t offset)
    {
        this->write<std::int32_t>(offset);
    }

    void write_offset64(const std::uint64_t offset)
    {
        this->write<std::uint64_t>(offset);
    }

    void write_count(std::uint16_t count) { this->write<std::uint16_t>(count); }

    template <typename T>
    void save_offset(T& val)
    {
        // Save the offset queued by queue_offset()
        // as our current position
        Offset offset {this->get_value_offset_data(val)};
        this->seek_and_write_relative_offset(offset.offset_pos,
                                             offset.value_pos);
    }

    template <typename T>
    void save_offsets(T& vals)
    {
        for (auto& val : vals)
            save_offset(val);
    }

    void set_string_table(std::vector<std::string> string_table)
    {
        m_string_table = string_table;
    }

    void queue_string_table_entry(std::string str)
    {
        for (auto& entry : m_string_table)
            if (entry == str)
                queue_offset32(entry);
    }

    template <typename T>
    void queue_offset32(T& val)
    requires(!std::is_pointer_v<T>)
    {
        // Queue an offset by saving its position and
        // the address of the value it's pointing to
        // as a pointer

        if (set_offset_pos(&val, this->tell())) {
            write_offset32(0);
            return;
        }

        Offset offset {this->tell(), &val};
        m_offsets.push_back(offset);

        write_offset32(0);
    }

    template <typename T>
    bool queue_value(T& val)
    {
        if (set_value_pos(&val, this->tell()))
            return true;

        Offset offset {0, &val, this->tell()};
        m_offsets.push_back(offset);
        return false;
    }

    template <typename T>
    void build_value(T& val)
    {
        queue_value(val);
        this->serialize_value(val);
    }

    template <typename T>
    void build_value_and_offset(T& val)
    {
        queue_value(val);
        this->serialize_value(val);
        save_offset(val);
    }

    template <typename T>
    void build_values(std::vector<T>& vals)
    {
        for (auto& val : vals)
            build_value(val);
    }

    template <typename T>
    void build_values_and_offsets(std::vector<T>& vals)
    {
        for (auto& val : vals)
            build_value_and_offset(val);
    }

    template <typename T>
    void build_dict(std::vector<T>& vals)
    {
        if (vals.size() > 0) {
            // Use the vector object as they key for
            // determining where to write the offset
            // to the dict
            queue_value(vals);

            this->write<std::uint32_t>((vals.size() + 1) * sizeof(ResNode<T>) +
                                       8);
            this->write<std::uint32_t>(vals.size());

            // First node. Always points to nothing
            this->write<std::uint32_t>(
                std::numeric_limits<std::uint32_t>::max());
            this->write<std::uint16_t>(1);
            this->write<std::uint16_t>(0);
            if (m_wiiu) {
                write_offset32(0);
                write_offset32(0);
            }
            else {
                write_offset64(0);
            }

            for (int i {0}; i < vals.size(); ++i) {
                this->write<std::uint32_t>(0); // ref_bit
                this->write<std::uint16_t>(i % 2 == 0 ? i
                                                      : i + 1); // left_index
                this->write<std::uint16_t>(i % 2 == 1 ? i
                                                      : i + 1); // right_index
                if (m_wiiu) {
                    write_offset32(0);       // name_offset
                    queue_offset32(vals[i]); // data_offset
                }
                else {
                    write_offset64(0); // name_offset
                }
            }
        }
    }

    template <typename T>
    void build_dict_and_offset(std::vector<T>& vals)
    {
        if (vals.size() > 0) {
            build_dict(vals);
            save_offset(vals);
        }
    }

    void build_string_table()
    {
        for (auto& res_str : m_string_table) {
            this->write<std::uint32_t>(res_str.size());

            if (queue_value(res_str))
                save_offset(res_str);

            this->write_string(res_str);
            align_up(4);
        }
    }

    bool set_value_pos(void* addr, std::size_t val_pos)
    {
        for (auto& offset : m_offsets)
            if (addr == offset.value_addr) {
                offset.value_pos = val_pos;
                return true;
            }

        return false;
    }

    bool set_offset_pos(void* addr, std::size_t offset_pos)
    {
        for (auto& offset : m_offsets)
            if (addr == offset.value_addr) {
                offset.offset_pos = offset_pos;
                return true;
            }
        return false;
    }

    bool is_wiiu() const { return m_wiiu; }
    void is_wiiu(bool val)
    {
        m_wiiu = val;
        if (m_wiiu)
            this->set_endianness(binaryio::endian::big);
        else
            this->set_endianness(binaryio::endian::little);
    }

    constexpr void align_up(size_t n)
    {
        auto offset {this->tell() + (n - this->tell() % n) % n};
        this->seek(offset);
    }

    int version() const { return m_version; }
    void version(int ver) { m_version = ver; }

    int version_major() const { return version() >> 24; }
    int version_major2() const { return version() >> 16 & 0xFF; }
    int version_minor() const { return version() >> 8 & 0xFF; }
    int version_micro() const { return version() & 0xFF; }

private:
    bool m_wiiu {false};
    std::uint32_t m_version;
    std::size_t m_saved_pos {0};

    struct Offset {
        std::size_t offset_pos;
        std::size_t value_pos;
        void* value_addr;

        Offset(std::size_t pos, void* ptr) : offset_pos(pos), value_addr(ptr) {}

        Offset(std::size_t pos, void* ptr, std::size_t val_pos)
            : offset_pos(pos), value_pos(val_pos), value_addr(ptr) {};

        bool operator==(const Offset& other)
        {
            return this->value_addr == other.value_addr;
        }
        bool operator!=(const Offset& other) { return !(*this == other); }
    };

    std::vector<Offset> m_offsets;
    std::vector<std::string> m_string_table;

    void save_pos() { m_saved_pos = this->tell(); }

    template <typename T>
    Offset get_value_offset_data(const T& val)
    {
        for (auto& offset : m_offsets)
            if (&val == offset.value_addr)
                return offset;

        no_offset_warning();
        return Offset {0, nullptr};
    }

    void seek_and_write_relative_offset(const std::size_t& offset_pos,
                                        const std::size_t& value_pos)
    {
        auto original_pos {this->tell()};
        this->seek(offset_pos);
        this->write_offset32(value_pos - offset_pos);
        this->seek(original_pos);
    }

    template <typename T>
    void serialize_value(T& val)
    {
        if constexpr (std::is_base_of_v<IResData, T>)
            val.serialize(*this);
        else if constexpr (is_container<T>)
            for (int i {0}; i < val.size(); ++i)
                serialize_value(val[i]);
        else
            this->write<T>(val);
    }

    void no_offset_warning()
    {
        std::cerr << "WARNING: using an offset without value" << '\n';
    }
};
} // namespace bfres

#endif