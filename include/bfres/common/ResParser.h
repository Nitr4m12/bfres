#include <algorithm>
#include <cstdint>
#include <iostream>
#include <memory>
#include <type_traits>

#include <binaryio/reader.h>

#ifndef BFRES_RESPARSER_H
#define BFRES_RESPARSER_H

namespace bfres {
class IResData;
// class ResFile;
class ResParser : public binaryio::BinaryReader {
public:
    ResParser(std::istream& is)
        : binaryio::BinaryReader(is, binaryio::endian::little) {};

    // ResParser(std::istream& is, std::shared_ptr<ResFile> file)
    //     : binaryio::BinaryReader(is, binaryio::endian::little)
    // {
    //     if (file)
    //         m_mainFile = file;
    //     else
    //         m_mainFile = std::shared_ptr<ResFile> {new ResFile()};
    // };

    void seek_cur(const std::size_t offset)
    {
        this->seek(offset, std::ios_base::cur);
    }

    std::int32_t read_offset32() { return this->read<std::int32_t>(); }
    std::uint64_t read_offset64() { return this->read<std::uint64_t>(); }
    std::uint16_t read_count() { return this->read<std::uint16_t>(); }

    template <typename T>
    T parse_offset()
    {
        T val;
        std::size_t original_pos {go_to_offset()};
        if (original_pos == 0)
            return val;

        deserialize_value(val);

        seek_after_offset(original_pos);
        return val;
    }
    template <typename T>
    T parse_offset(const std::size_t& offset)
    {
        T val;
        std::size_t original_pos {go_to_offset(offset)};
        if (original_pos == 0)
            return val;

        deserialize_value(val);

        this->seek(original_pos);
        return val;
    }

    std::string parse_string_offset()
    {
        std::string str_val;

        std::size_t original_pos {go_to_offset()};
        if (original_pos == 0)
            return str_val;

        parse_string(str_val);

        seek_after_offset(original_pos);
        return str_val;
    }
    std::string parse_string_offset(const std::size_t& offset)
    {
        std::string str_val;

        std::size_t original_pos {go_to_offset(offset)};
        if (original_pos == 0)
            return str_val;

        parse_string(str_val);

        this->seek(original_pos);
        return str_val;
    }

    template <typename T>
    std::vector<T> parse_vector(const int& count)
    {
        std::vector<T> vals;

        std::size_t original_pos {go_to_offset()};
        if (original_pos == 0)
            return vals;

        vals.resize(count);
        for (int i {0}; i < vals.size(); ++i)
            deserialize_value(vals[i]);

        seek_after_offset(original_pos);
        return vals;
    }
    template <typename T>
    std::vector<T> parse_vector(const int& count, const std::size_t& offset)
    {
        std::vector<T> vals;

        std::size_t original_pos {go_to_offset(offset)};
        if (original_pos == 0)
            return vals;

        vals.resize(count);
        for (int i {0}; i < vals.size(); ++i)
            deserialize_value(vals[i]);

        this->seek(original_pos);
        return vals;
    }

    std::vector<std::string> parse_vector_of_strings(const int& count)
    {
        std::vector<std::string> strings;

        std::size_t original_pos {go_to_offset()};
        if (original_pos == 0)
            return strings;

        strings.resize(count);
        for (int i {0}; i < strings.size(); ++i)
            strings[i] = this->read_string();

        seek_after_offset(original_pos);
        return strings;
    }

    std::vector<std::string> parse_vector_of_strings(const int& count,
                                                     const std::size_t& offset)
    {
        std::vector<std::string> strings;

        std::size_t original_pos {go_to_offset(offset)};
        if (original_pos == 0)
            return strings;

        strings.resize(count);
        for (int i {0}; i < strings.size(); ++i)
            strings[i] = this->read_string();

        this->seek(original_pos);
        return strings;
    }

    template <typename T>
    std::vector<T> parse_offset_and_dict()
    {
        std::vector<T> vals;
        auto to_vals {this->read<std::uint64_t>()};
        auto original_pos {go_to_offset()};
        if (original_pos == 0)
            return vals;

        parse_dict(vals, to_vals);

        seek_after_offset(original_pos);
        return vals;
    }

    template <typename T>
    std::vector<T> parse_dict_offset()
    {
        std::vector<T> vals;

        std::size_t original_pos {go_to_offset()};
        if (original_pos == 0)
            return vals;

        parse_dict(vals);

        seek_after_offset(original_pos);
        return vals;
    }
    template <typename T>
    std::vector<T> parse_dict_offset(const std::size_t& vals_offset)
    {
        std::vector<T> vals;

        std::size_t original_pos {go_to_offset()};
        if (original_pos == 0)
            return vals;

        parse_dict(vals, vals_offset);

        seek_after_offset(original_pos);
        return vals;
    }
    template <typename T>
    std::vector<T> parse_dict_offset(const std::size_t& vals_offset,
                                     const std::size_t& offset)
    {
        std::vector<T> vals;

        std::size_t original_pos {go_to_offset(offset)};
        if (original_pos == 0)
            return vals;

        parse_dict(vals, vals_offset);

        this->seek(original_pos);
        return vals;
    }

    int version() const { return m_version; }
    void version(int ver) { m_version = ver; }

    int version_major() const { return version() >> 24; }
    int version_major2() const { return version() >> 16 & 0xFF; }
    int version_minor() const { return version() >> 8 & 0xFF; }
    int version_micro() const { return version() & 0xFF; }

    // bool ptr_set()
    // {
    //     if (!m_mainFile)
    //         return false;
    //     return true;
    // }

    // auto get_file_ptr() const { return m_mainFile; }

    std::string name() const { return m_name; }
    void name(std::string n) { m_name = n; }

    bool is_wiiu() const { return m_wiiu; }
    void is_wiiu(bool val)
    {
        m_wiiu = val;
        if (m_wiiu)
            this->set_endianness(binaryio::endian::big);
        else
            this->set_endianness(binaryio::endian::little);
    }

    std::vector<std::string> string_table() { return m_string_table; }

private:
    std::string m_name;
    std::uint32_t m_version;
    std::vector<std::string> m_string_table;

    bool m_wiiu {false};
    // std::shared_ptr<ResFile> m_mainFile {nullptr};

    std::size_t go_to_offset()
    {
        std::size_t original_pos {this->tell()};
        if (m_wiiu) {
            auto offset {this->read<std::int32_t>()};
            if (offset == -1 || offset == 0) {
                this->seek(original_pos + offset_size());
                return 0;
            }

            this->seek(original_pos + offset);
        }
        else {
            auto offset {this->read<std::uint64_t>()};
            if (offset == -1 || offset == 0) {
                this->seek(original_pos + offset_size());
                return 0;
            }

            this->seek(offset);
        }
        return original_pos;
    }
    std::size_t go_to_offset(const std::size_t& abs_offset)
    {
        std::size_t original_pos {this->tell()};
        if (abs_offset == 0)
            return 0;
        this->seek(abs_offset);
        return original_pos;
    }

    std::size_t offset_size() const
    {
        if (m_wiiu)
            return sizeof(std::int32_t);
        else
            return sizeof(std::uint64_t);
    }

    void seek_after_offset(const std::size_t& original_pos)
    {
        this->seek(original_pos + offset_size());
    }

    template <typename T>
    void deserialize_value(T& val)
    {
        if constexpr (std::is_base_of_v<IResData, T>)
            val.deserialize(*this);
        else
            val = this->read<T>();
    }

    void parse_string(std::string& str_val)
    {
        std::uint32_t str_size;

        if (m_wiiu) {
            this->seek(this->tell() - sizeof(std::uint32_t));
            str_size = this->read<std::uint32_t>();
        }
        else {
            str_size = this->read<std::uint16_t>();
        }

        str_val = this->read_string(str_size);

        if (std::find(m_string_table.begin(), m_string_table.end(), str_val)
            == m_string_table.end())
            m_string_table.push_back(str_val);
    }

    template <typename T>
    void parse_dict()
    {
        auto dict_size {this->read<std::uint32_t>()};
        auto num_nodes {this->read<std::int32_t>()};

        // First value always points to nothing
        auto ref_bit {this->read<std::uint32_t>()};
        auto left_index {this->read<std::uint16_t>()};
        auto right_index {this->read<std::uint16_t>()};
        auto name_offset {this->read<std::int32_t>()};

        for (int i {0}; i < num_nodes + 1; ++i) {
            auto ref_bit {this->read<std::uint32_t>()};
            auto left_index {this->read<std::uint16_t>()};
            auto right_index {this->read<std::uint16_t>()};
            auto name_offset {this->read<std::int32_t>()};
        }
    }
    template <typename T>
    void parse_dict(std::vector<T>& vals)
    {
        auto dict_size = this->read<std::uint32_t>();
        auto num_nodes = this->read<std::int32_t>();
        vals.resize(num_nodes);

        // First value always points to nothing
        auto ref_bit {this->read<std::uint32_t>()};
        auto left_index {this->read<std::uint16_t>()};
        auto right_index {this->read<std::uint16_t>()};
        auto name_offset {this->read<std::int32_t>()};
        auto null_vals_offset {this->read<std::int32_t>()};

        if (m_wiiu) {
            for (int i {0}; i < vals.size(); ++i) {
                auto ref_bit {this->read<std::uint32_t>()};
                auto left_index {this->read<std::uint16_t>()};
                auto right_index {this->read<std::uint16_t>()};
                auto name_offset {this->read<std::int32_t>()};
                vals[i] = parse_offset<T>();
            }
        }
    }
    template <typename T>
    void parse_dict(std::vector<T>& vals, const std::size_t& vals_offset)
    {
        auto dict_size = this->read<std::uint32_t>();
        auto num_nodes = this->read<std::int32_t>();
        vals.resize(num_nodes);

        // First value always points to nothing
        auto ref_bit {this->read<std::uint32_t>()};
        auto left_index {this->read<std::uint16_t>()};
        auto right_index {this->read<std::uint16_t>()};
        auto name_offset {this->read<std::uint64_t>()};
        auto null_vals_offset {this->read<std::uint64_t>()};

        for (int i {0}; i < vals.size(); ++i) {
            auto ref_bit {this->read<std::uint32_t>()};
            auto left_index {this->read<std::uint16_t>()};
            auto right_index {this->read<std::uint16_t>()};
            auto name_offset {this->read<std::uint64_t>()};
            if (vals_offset != 0)
                vals[i] = parse_offset<T>(vals_offset);
        }
    }
};

} // namespace bfres
#endif