#include <cstdint>
#include <variant>

#include <binaryio/type_utils.h>

#include "bfres/common/IResData.h"

#ifndef BFRES_RESUSERDATA_H
#define BFRES_RESUSERDATA_H

namespace bfres {
struct UserDataStruct {
    std::uint32_t size;
    std::uint8_t value[4];

    BINARYIO_DEFINE_FIELDS(UserDataStruct, size, value);
};

class ResUserData : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::variant<std::vector<std::int32_t>, std::vector<float>,
                 std::vector<std::string>, std::vector<UserDataStruct>>
        m_data;
};
} // namespace bfres

#endif