#include <array>
#include <cstdint>
#include <string>
#include <vector>

#include <binaryio/type_utils.h>

#include "math/Vector.h"

#include "bfres/common/IResData.h"

#ifndef BFRES_RESCOMMON_H
#define BFRES_RESCOMMON_H

namespace bfres {
const int32_t BYTE_ORDER_MARK = 0xFEFF;

struct FixedPoint16 {
    std::int16_t val;
};

struct Color {
    float r;
    float g;
    float b;
    BINARYIO_DEFINE_FIELDS(Color, r, g, b);
};

struct BinaryFileHeader64 {
    std::array<std::uint8_t, 8> signature;
    std::uint32_t version;
    std::uint16_t byteOrder;
    std::uint8_t alignment;
    std::uint8_t targetAddressSize;
    std::uint32_t toFileName;
    std::uint16_t flags;
    std::uint16_t toFirstBlock;
    std::uint32_t toRelocTable;
    std::uint32_t fileSize;

    BINARYIO_DEFINE_FIELDS(BinaryFileHeader64, signature, version, byteOrder, alignment,
                           targetAddressSize, toFileName, flags, toFirstBlock, toRelocTable,
                           fileSize);
};

struct BinaryFileHeader {
    std::array<std::uint8_t, 4> signature; // std::uint32_t sigWord
    std::uint32_t version;
    std::uint16_t byteOrder;
    std::uint16_t headerSize;
    std::uint32_t fileSize;

    BINARYIO_DEFINE_FIELDS(BinaryFileHeader, signature, version, byteOrder, headerSize, fileSize);
};

struct BinaryBlockHeader64 {
    std::array<std::uint8_t, 4> signature;
    std::uint32_t toNextBlock;
    std::uint32_t blockSize;
    std::uint32_t pad;

    BINARYIO_DEFINE_FIELDS(BinaryBlockHeader64, signature, toNextBlock, blockSize);
};

struct BinaryBlockHeader {
    std::uint8_t signature[4]; // std::uint32_t sigWord

    BINARYIO_DEFINE_FIELDS(BinaryBlockHeader, signature)
};

struct ResExternalFileData {
    std::int32_t offset;
    std::uint32_t size;

    BINARYIO_DEFINE_FIELDS(ResExternalFileData, offset, size);
};

struct ResString64 {
    std::uint16_t size;
    std::string value;
};

} // namespace bfres
#endif