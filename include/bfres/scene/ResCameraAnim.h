#include "bfres/common/ResAnimCurve.h"
#include "bfres/common/ResUserData.h"

#include "bfres/scene/ResCameraAnimResult.h"

#ifndef BFRES_SCENE_RESCAMERAANIM_H
#define BFRES_SCENE_RESCAMERAANIM_H

namespace bfres::scene {
class ResCameraAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::uint16_t m_flags;
    std::uint32_t m_bakedSize;

    std::string m_name;

    std::vector<ResAnimCurve> m_curves;
    CameraAnimResult m_baseValue;
    std::vector<ResUserData> m_userData;
};
} // namespace bfres::scene

#endif
