#include "bfres/common/ResCommon.h"

#ifndef BFRES_SCENE_RESLIGHTANIMRESULT_H
#define BFRES_SCENE_RESLIGHTANIMRESULT_H

namespace bfres::scene {
struct LightAnimResult {
    std::int32_t enabled;
    Vector3f position;
    Vector3f rotation;
    Vector2f distanceAttenuation;
    Vector2f angleAttenuation;

    Color firstColor;
    Color secondColor;

    BINARYIO_DEFINE_FIELDS(LightAnimResult, enabled, position, rotation,
                           distanceAttenuation, angleAttenuation, firstColor,
                           secondColor);
};
}

#endif