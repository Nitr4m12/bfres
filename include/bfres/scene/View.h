#include <binaryio/type_utils.h>

#include "math/Vector.h"

#ifndef BFRES_SCENE_CAMERAANIM_VIEW_H
#define BFRES_SCENE_CAMERAANIM_VIEW_H

namespace bfres::scene {
struct View {
    Vector3f pos;
    Vector3f rotation;
    float twist;

    BINARYIO_DEFINE_FIELDS(View, pos, rotation, twist);
};
} // namespace bfres::scene

#endif