#include "bfres/common/ResCommon.h"

#ifndef BFRES_SCENE_RESFOGANIMRESULT_H
#define BFRES_SCENE_RESFOGANIMRESULT_H

namespace bfres::scene {
struct FogAnimResult {
    Vector2f distanceAttenuation;
    Color fogColor;

    BINARYIO_DEFINE_FIELDS(FogAnimResult, distanceAttenuation, fogColor);
};
} // namespace bfres::scene

#endif