#include "bfres/scene/Projection.h"
#include "bfres/scene/View.h"

#ifndef BFRES_SCENE_RESCAMERAANIMRESULT_H
#define BFRES_SCENE_RESCAMERAANIMRESULT_H

namespace bfres::scene {
struct CameraAnimResult {
    Projection projection;
    View view;

    BINARYIO_DEFINE_FIELDS(CameraAnimResult, projection, view);
};
}

#endif