#include "bfres/common/ResAnimCurve.h"
#include "bfres/common/ResUserData.h"

#include "bfres/scene/ResFogAnimResult.h"

#ifndef BFRES_SCENE_RESFOGANIM_H
#define BFRES_SCENE_RESFOGANIM_H

namespace bfres::scene {
class ResFogAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;

    std::uint16_t m_flags;
    std::uint32_t m_bakedSize;
    std::int8_t m_distAttenFuncIndex;

    std::vector<ResAnimCurve> m_curves;
    FogAnimResult m_baseValue;
    std::vector<ResUserData> m_userData;
};
} // namespace bfres::scene

#endif