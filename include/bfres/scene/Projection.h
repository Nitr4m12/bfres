#include <binaryio/type_utils.h>

#ifndef BFRES_SCENE_PROJECTION_H
#define BFRES_SCENE_PROJECTION_H

namespace bfres::scene {
struct Projection {
    float nearZ;
    float farZ;
    float aspect;
    float height; // or fov

    BINARYIO_DEFINE_FIELDS(Projection, nearZ, farZ);
};
} // namespace bfres::scene

#endif