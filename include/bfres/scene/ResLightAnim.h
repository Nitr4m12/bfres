#include "bfres/common/ResAnimCurve.h"
#include "bfres/common/ResUserData.h"

#include "bfres/scene/ResLightAnimResult.h"

#ifndef BFRES_SCENE_RESLIGHTANIM_H
#define BFRES_SCENE_RESLIGHTANIM_H

namespace bfres::scene {
class ResLightAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::string m_lightTypeName;
    std::string m_distAttenFuncName;
    std::string m_angleAttenFuncName;

    std::uint16_t m_flags;
    std::int8_t m_lightType;
    std::uint32_t m_bakedSize;
    std::int8_t m_distAttenFuncIndex;
    std::int8_t m_angleAttenFuncIndex;

    std::vector<ResAnimCurve> m_curves;
    LightAnimResult m_baseValue;
    std::vector<ResUserData> m_userData;
};
} // namespace bfres::scene

#endif