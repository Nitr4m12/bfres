#include "bfres/common/ResAnimCurve.h"
#include "bfres/common/ResDictionary.h"

#include "bfres/scene/ResCameraAnim.h"
#include "bfres/scene/ResFogAnim.h"
#include "bfres/scene/ResLightAnim.h"

#ifndef BFRES_RESSCENEANIM_H
#define BFRES_RESSCENEANIM_H

namespace bfres::scene {
class ResSceneAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::string m_path;

    std::vector<scene::ResCameraAnim> m_cameraAnims;
    std::vector<scene::ResLightAnim> m_lightAnims;
    std::vector<scene::ResFogAnim> m_fogAnims;
    std::vector<ResUserData> m_userData;
};
} // namespace bfres

#endif