#include <cstdint>

#include <binaryio/type_utils.h>

#ifndef BFRES_SHAPE_RESKEYSHAPE_H
#define BFRES_SHAPE_RESKEYSHAPE_H

namespace bfres::shape {
struct ResKeyShape {
    std::uint8_t targetAttribIndices[20];
    std::uint8_t toTargetAttribIndex[4];

    BINARYIO_DEFINE_FIELDS(ResKeyShape, targetAttribIndices,
                           toTargetAttribIndex);
};
}

#endif