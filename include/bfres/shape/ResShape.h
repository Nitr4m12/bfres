#include "bfres/common/IResData.h"
#include "bfres/common/ResBuffer.h"
#include "bfres/common/ResDictionary.h"

#include "bfres/shape/ResBounding.h"
#include "bfres/shape/ResBoundingNode.h"
#include "bfres/shape/ResKeyShape.h"
#include "bfres/shape/ResMesh.h"
#include "bfres/vertex/ResVertex.h"

#ifndef BFRES_SHAPE_RESSHAPE_H
#define BFRES_SHAPE_RESSHAPE_H

namespace bfres::shape {
class ResShape : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;

    std::uint32_t m_flags;
    std::uint16_t m_index;
    std::uint16_t m_materialIndex;
    std::uint16_t m_boneIndex;
    std::uint16_t m_vertexIndex;
    std::uint8_t m_vtxSkinCount;
    float m_radius;

    vertex::ResVertex m_vertex;
    std::vector<ResMesh> m_meshes;
    std::vector<std::uint16_t> m_skinBoneIndices;
    std::vector<ResKeyShape> m_keyShapes;
    std::vector<ResBoundingNode> m_boundingNodes;
    std::vector<ResBounding> m_subBoundings;
    std::vector<std::uint16_t> m_subMeshIndices;
};
} // namespace bfres

#endif