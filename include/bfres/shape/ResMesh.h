#include "gx2/enum.h"

#include "bfres/common/IResData.h"

#include "bfres/vertex/ResVertexBuffer.h"
#include "bfres/shape/ResSubMesh.h"

#ifndef BFRES_SHAPE_RESMESH_H
#define BFRES_SHAPE_RESMESH_H

namespace bfres::shape {
class ResMesh : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

    auto& sub_meshes() { return m_subMeshes; }
    auto& index_buffer() { return m_indexBuffer; }

private:
    GX2PrimitiveMode m_primMode;
    GX2IndexType m_type;

    std::vector<ResSubMesh> m_subMeshes;
    vertex::ResVertexBuffer m_indexBuffer;
};
} // namespace bfres

#endif