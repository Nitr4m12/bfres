#include <cstdint>

#include <binaryio/type_utils.h>

#ifndef BFRES_SHAPE_RESSUBMESH_H
#define BFRES_SHAPE_RESSUBMESH_H

namespace bfres::shape {
struct ResSubMesh {
    std::uint32_t offset;
    std::uint32_t count;

    BINARYIO_DEFINE_FIELDS(ResSubMesh, offset, count);
};
}

#endif