#include <cstdint>

#include <binaryio/type_utils.h>

#ifndef BFRES_SHAPE_RESBOUNDINGNODE_H
#define BFRES_SHAPE_RESBOUNDINGNODE_H

namespace bfres::shape {
struct ResBoundingNode {
    std::uint16_t leftIndex;
    std::uint16_t rightIndex;
    std::uint16_t prevIndex;
    std::uint16_t nextIndex;
    std::uint16_t subMeshIndex;
    std::uint16_t numSubMesh;

    BINARYIO_DEFINE_FIELDS(ResBoundingNode, leftIndex, rightIndex, prevIndex,
                           nextIndex, subMeshIndex, numSubMesh);
};
}

#endif