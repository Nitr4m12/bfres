#include <binaryio/type_utils.h>

#include "math/Vector.h"

#ifndef BFRES_SHAPE_RESBOUNDING_H
#define BFRES_SHAPE_RESBOUNDING_H

namespace bfres::shape {
struct ResBounding {
    Vector3f center;
    Vector3f extent;

    BINARYIO_DEFINE_FIELDS(ResBounding, center, extent);
};
} // namespace bfres::shape

#endif