#include <cstdint>

#ifndef BFRES_TEXTURE_RESTEXTUREREF_H
#define BFRES_TEXTURE_RESTEXTUREREF_H

namespace bfres::texture {
struct ResTextureRef {
    std::int32_t toName;
    std::int32_t toRefData;
};
} // namespace bfres::texture

#endif