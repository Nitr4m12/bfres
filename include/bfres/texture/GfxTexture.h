#include <cstdint>

#include <binaryio/type_utils.h>

#include "gx2/enum.h"

#ifndef BFRES_TEXTURE_GFXTEXTURE_H
#define BFRES_TEXTURE_GFXTEXTURE_H

namespace bfres::texture {
struct GX2Surface {
    GX2SurfaceDim dim;
    std::uint32_t width;
    std::uint32_t height;
    std::uint32_t depth;
    std::uint32_t mipCount;
    GX2SurfaceFormat format;
    GX2AAMode aaMode;
    GX2SurfaceUse use;
    std::uint32_t imageSize;
    std::uint32_t imagePointer;
    std::uint32_t mipMapSize;
    std::uint32_t mipMapPointer;
    GX2TileMode tileMode;
    std::uint32_t swizzle;
    std::uint32_t alignment;
    std::uint32_t pitch;
    std::uint32_t mipLevelOffset[13];
    BINARYIO_DEFINE_FIELDS(GX2Surface, dim, width, height, depth, mipCount, format, aaMode, use,
                           imageSize, imagePointer, mipMapSize, mipMapPointer, tileMode, swizzle,
                           alignment, pitch, mipLevelOffset);
};

struct GfxTexture {
    GX2Surface surface;
    std::uint32_t viewFirstMip;
    std::uint32_t viewNumMips;
    std::uint32_t viewFirstSlice;
    std::uint32_t viewNumSlices;
    std::uint32_t compMap;

    std::uint32_t regs[5];

    BINARYIO_DEFINE_FIELDS(GfxTexture, surface, viewFirstMip, viewNumMips, viewFirstSlice,
                           viewNumSlices, compMap, regs);
};
} // namespace bfres::texture

#endif