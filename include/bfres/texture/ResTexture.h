#include "bfres/common/ResDictionary.h"
#include "bfres/common/ResUserData.h"

#include "bfres/texture/GfxTexture.h"

#ifndef BFRES_TEXTURE_RESTEXTURE_H
#define BFRES_TEXTURE_RESTEXTURE_H

namespace bfres::texture {
class ResTexture : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    GfxTexture m_gfxTexture;

    std::uint32_t m_handle;
    std::string m_name;
    std::string m_path;

    std::vector<std::uint8_t> m_data;
    std::vector<std::uint8_t> m_mipMapData;
    std::vector<ResUserData> m_userData;
};

} // namespace bfres::texture

#endif