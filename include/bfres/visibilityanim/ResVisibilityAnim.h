#include "bfres/common/IResData.h"
#include "bfres/common/ResAnimCurve.h"
#include "bfres/common/ResUserData.h"

#include "bfres/model/ResModel.h"

#ifndef BFRES_RESVISIBILITYANIM_H
#define BFRES_RESVISIBILITYANIM_H

namespace bfres::visanim {
class ResVisibilityAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::string m_path;

    std::uint16_t m_flags;
    std::uint32_t m_bakedSize;

    model::ResModel m_bindModel;
    std::vector<std::uint16_t> m_bindIndices;
    std::vector<std::string> m_names;
    std::vector<ResAnimCurve> m_curves;
    std::vector<std::uint8_t> m_baseValues;
    std::vector<ResUserData> m_userData;
};

} // namespace bfres::visanim

#endif