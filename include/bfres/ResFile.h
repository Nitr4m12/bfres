#include <array>
#include <fstream>
#include <sstream>
#include <string>

#include <binaryio/reader.h>
#include <binaryio/writer.h>

#include "bfres/common/ResCommon.h"
#include "bfres/common/ResDictionary.h"

#include "bfres/materialanim/ResMaterialAnimArchive.h"
#include "bfres/materialanim/ResShaderParamAnim.h"
#include "bfres/materialanim/ResTexturePatternAnim.h"
#include "bfres/model/ResModel.h"
#include "bfres/scene/ResSceneAnim.h"
#include "bfres/shapeanim/ResShapeAnim.h"
#include "bfres/skeletalanim/ResSkeletalAnim.h"
#include "bfres/texture/ResTexture.h"
#include "bfres/visibilityanim/ResVisibilityAnim.h"

#ifndef BFRES_RESFILE_H
#define BFRES_RESFILE_H

namespace bfres {
struct ResFileHeader64 : public BinaryFileHeader64 {
    std::uint64_t toName;

    std::uint64_t toModels;
    std::uint64_t toModelDict;
    std::uint64_t toSkeletalAnims;
    std::uint64_t toSkeletalAnimDict;
    std::uint64_t toMaterialAnims;
    std::uint64_t toMaterialAnimsDict;
    std::uint64_t toBoneVisibilityAnims;
    std::uint64_t toBoneVisibilityAnimDict;
    std::uint64_t toShapeAnims;
    std::uint64_t toShapeAnimDict;
    std::uint64_t toSceneAnims;
    std::uint64_t toSceneAnimData;

    std::uint64_t memoryPool;
    std::uint64_t bufferSection;

    std::uint64_t toEmbeddedFiles;
    std::uint64_t toEmbeddedFileDict;

    std::uint64_t padding;

    std::uint64_t toStrTable;
    std::uint32_t strTableSize;

    std::uint16_t modelCount;
    std::uint16_t skeletonAnimCount;
    std::uint16_t materialAnimCount;
    std::uint16_t boneVisAnimCount;
    std::uint16_t shapeAnimCount;
    std::uint16_t sceneAnimCount;
    std::uint16_t externalFileCount;
    std::uint8_t padding2[0x6];
};

class ResFile : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void deserialize(std::string file_name);

    void serialize(ResBuilder& builder) override;
    void serialize();

    void dump(std::ostringstream& oss) { oss << "File name: " << m_name; }

    std::uint32_t version() const { return m_version; }
    std::string name() const { return m_name; }

    auto models() const { return m_models; };
    auto textures() const { return m_textures; };
    auto skeletal_anims() const { return m_skeletalAnims; };
    auto shader_param_anims() const { return m_shaderParamAnims; };
    auto color_anims() const { return m_colorAnims; };
    auto texture_srt_anims() const { return m_textureSrts; };
    auto texture_pattern_anims() const { return m_texturePatternAnims; };
    auto bone_vis_anims() const { return m_boneVisAnims; };
    auto mat_vis_anims() const { return m_matVisAnims; };
    auto shape_anims() const { return m_shapeAnims; };
    auto scene_anims() const { return m_sceneAnims; };
    auto external_files() const { return m_externalFiles; };

    auto string_table() const { return m_stringTable; }

private:
    std::uint32_t m_version;

    std::string m_name;
    std::vector<model::ResModel> m_models;
    std::vector<texture::ResTexture> m_textures;
    std::vector<skeletalanim::ResSkeletalAnim> m_skeletalAnims;
    std::vector<materialanim::ResMaterialAnimArchive> m_matAnimArchives;
    std::vector<materialanim::ResShaderParamAnim> m_shaderParamAnims;
    std::vector<materialanim::ResShaderParamAnim> m_colorAnims;
    std::vector<materialanim::ResShaderParamAnim> m_textureSrts;
    std::vector<materialanim::ResTexturePatternAnim> m_texturePatternAnims;
    std::vector<visanim::ResVisibilityAnim> m_boneVisAnims;
    std::vector<visanim::ResVisibilityAnim> m_matVisAnims;
    std::vector<shapeanim::ResShapeAnim> m_shapeAnims;
    std::vector<scene::ResSceneAnim> m_sceneAnims;
    std::vector<ResExternalFileData> m_externalFiles;

    // TODO: Find a better way of handling the string table
    std::vector<std::string> m_stringTable;
};

} // namespace bfres

#endif