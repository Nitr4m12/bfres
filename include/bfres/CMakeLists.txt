add_subdirectory(common)
add_subdirectory(material)
add_subdirectory(materialanim)
add_subdirectory(model)
add_subdirectory(scene)
add_subdirectory(shape)
add_subdirectory(shapeanim)
add_subdirectory(skeletalanim)
add_subdirectory(skeleton)
add_subdirectory(texture)
add_subdirectory(vertex)
add_subdirectory(visibilityanim)

target_sources(bfres PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/ResFile.h
)
