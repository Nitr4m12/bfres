#include "bfres/common/IResData.h"

#include "bfres/common/ResDictionary.h"
#include "bfres/skeleton/ResBone.h"

#ifndef BFRES_SKELETON_RESSKELETON_H
#define BFRES_SKELETON_RESSKELETON_H

namespace bfres::skeleton {
class ResSkeleton : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::uint32_t m_flags;
    std::vector<ResBone> m_bones;
    std::vector<std::uint16_t> m_matrixToBoneTable;
    std::vector<Matrix34f> m_inverseModelMatrices;
};
} // namespace bfres::skeleton

#endif