#include <variant>

#include "math/Matrix.h"
#include "math/Vector.h"

#include "bfres/common/IResData.h"
#include "bfres/common/ResUserData.h"

#ifndef BFRES_SKELETON_RESBONE_H
#define BFRES_SKELETON_RESBONE_H

namespace bfres::skeleton {
class ResBone : public IResData {
public:
    void deserialize(ResParser& parser) override;
    // void serialize(ResBuilder& builder) override;

    void serialize(ResBuilder& builder, int index);

    const Vector3f& scale() { return m_scale; }
    const Vector4f& rotation() { return m_rotation; }
    const Vector3f& translation() { return m_translation; }

private:
    std::string m_name;

    std::uint16_t m_index;
    std::uint16_t m_parentIndex;
    std::int16_t m_smoothMtxIndex;
    std::int16_t m_rigidMtxIndex;
    std::uint16_t m_billboardIndex;

    std::uint32_t m_flags;
    Vector3f m_scale;
    Vector4f m_rotation;
    Vector3f m_translation;

    std::vector<ResUserData> m_userData;

    Matrix34f m_invModelMtx;
};
} // namespace bfres::skeleton

#endif