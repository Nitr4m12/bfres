#include "math/Vector.h"

#include "bfres/common/IResData.h"

#ifndef BFRES_SKELETALANIM_RESBONEANIMRESULT_H
#define BFRES_SKELETALANIM_RESBONEANIMRESULT_H

namespace bfres::skeletalanim {
class ResBoneAnimResult : public IResData {
public:
    // Requires custom parsing from ResBoneAnim
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

    void flags(int new_flags) { m_flags = new_flags; }

    auto scale() { return m_scale; };
    auto rotation() { return m_rotation; };
    auto translation() { return m_translation; };

private:
    std::uint32_t m_flags;

    Vector3f m_scale;
    Vector4f m_rotation;
    Vector3f m_translation;
};
}

#endif