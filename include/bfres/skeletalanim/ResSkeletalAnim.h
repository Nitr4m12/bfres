#include "bfres/common/IResData.h"
#include "bfres/common/ResUserData.h"

#include "bfres/skeletalanim/ResBoneAnim.h"
#include "bfres/skeleton/ResSkeleton.h"

#ifndef BFRES_SKELETALANIM_RESSKELETALANIM_H
#define BFRES_SKELETALANIM_RESSKELETALANIM_H

namespace bfres::skeletalanim {
class ResSkeletalAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

private:
    std::string m_name;
    std::string m_path;

    std::uint32_t m_flags;
    std::uint32_t m_bakedSize;

    std::vector<ResBoneAnim> m_boneAnims;
    skeleton::ResSkeleton m_bindSkeleton;
    std::vector<std::int16_t> m_bindIndices;
    std::vector<ResUserData> m_userData;
};

} // namespace bfres::skeletalanim

#endif