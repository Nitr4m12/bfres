#include "bfres/common/ResAnimCurve.h"
#include "bfres/skeletalanim/ResBoneAnimResult.h"

#ifndef BFRES_SKELETALANIM_RESBONEANIM_H
#define BFRES_SKELETALANIM_RESBONEANIM_H

namespace bfres::skeletalanim {
class ResBoneAnim : public IResData {
public:
    void deserialize(ResParser& parser) override;
    void serialize(ResBuilder& builder) override;

    auto curves() { return m_curves; }
    auto base_value() { return m_baseValue; }

private:
    std::uint32_t m_flags;

    std::string m_name;

    std::uint8_t m_initialRotation;
    std::uint8_t m_initialTranslation;
    std::uint8_t m_baseTranslateOffset;
    std::int32_t m_startCurveIndex;

    std::vector<ResAnimCurve> m_curves;
    ResBoneAnimResult m_baseValue;
};
} // namespace bfres::skeletalanim

#endif