#include <cstdint>

#ifndef MATH_MATRIX_H
#define MATH_MATRIX_H

template <typename T, std::size_t IO, std::size_t I>
struct Matrix {
    T a[IO][I];
};
using Matrix34f = Matrix<float, 3, 4>;

#endif