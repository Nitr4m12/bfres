#include <binaryio/type_utils.h>

#ifndef MATH_VECTOR_H
#define MATH_VECTOR_H

template <typename T>
struct Vector2 {
    T x;
    T y;
    BINARYIO_DEFINE_FIELDS(Vector2<T>, x, y);
};
using Vector2f = Vector2<float>;
using Vector2i = Vector2<int>;

template <typename T>
struct Vector3 {
    T x;
    T y;
    T z;
    BINARYIO_DEFINE_FIELDS(Vector3<T>, x, y, z);
};
using Vector3f = Vector3<float>;

template <typename T>
struct Vector4 {
    T x;
    T y;
    T z;
    T w;
    BINARYIO_DEFINE_FIELDS(Vector4<T>, x, y, z, w);
};
using Vector4f = Vector4<float>;
using Quaternion = Vector4<float>;

#endif