#include <iostream>
#include <sstream>
#include <fstream>

#include "bfres/ResFile.h"

int main(int argc, char** argv)
try {
    bfres::ResFile res_file;
    res_file.deserialize(argv[1]);

    std::ostringstream oss;
    res_file.dump(oss);

    res_file.serialize();

    std::cout << oss.str() << '\n';
}
catch (std::runtime_error& err) {
    std::cerr << err.what() << '\n';
    return 1;
}